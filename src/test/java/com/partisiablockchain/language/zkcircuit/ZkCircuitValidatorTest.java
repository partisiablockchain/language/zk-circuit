package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/** Unit test of ZK circuit validation. */
public final class ZkCircuitValidatorTest {

  static List<ExampleCircuits.TestCase> testCases() {
    return ExampleCircuits.testCases();
  }

  @ParameterizedTest
  @MethodSource("testCases")
  void validateExampleCircuits(ExampleCircuits.TestCase testCase) {
    assertValid(testCase.circuit(), testCase.expectedOutputTypes());
  }

  @Test
  void testInvalidUnknownGate() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    // Sub-gate does not exist!
    final ZkAddress root =
        circuit.add(
            ZkType.SBI32,
            new ZkOperation.Unary(ZkOperation.UnaryOp.ARITH_NEGATE_SIGNED, new ZkAddress(123)));
    circuit.setRoots(List.of(root));
    assertInvalid(circuit, List.of(ZkType.SBI32), "Gate 0  : Reference to unknown gate 123");
  }

  @Test
  void testInvalidNullOperation() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    Assertions.assertThatThrownBy(() -> circuit.add(ZkType.SBI0, null))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  void testInvalidNullType() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    Assertions.assertThatThrownBy(
            () -> circuit.add(null, ZkOperation.Constant.fromInt(ZkType.SBOOL, 1)))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  void testInvalidNullRoots() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    Assertions.assertThatThrownBy(() -> circuit.setRoots(null))
        .isInstanceOf(NullPointerException.class);

    final List<ZkAddress> roots = new ArrayList<>();
    roots.add(null);
    roots.add(null);
    Assertions.assertThatThrownBy(() -> circuit.setRoots(roots))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  void testInvalidUnknownRoots() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    final ZkAddress sub =
        circuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 123));
    circuit.add(ZkType.SBI32, new ZkOperation.Unary(ZkOperation.UnaryOp.ARITH_NEGATE_SIGNED, sub));

    circuit.setRoots(List.of(new ZkAddress(9999)));

    assertInvalid(circuit, List.of(ZkType.SBI32), "Circuit : Reference to unknown gate 9999");
  }

  @Test
  void testInvalidTypeCount() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    // Sub-gate does not exist!
    final ZkAddress sub =
        circuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 123));
    final ZkAddress root =
        circuit.add(
            ZkType.SBI32, new ZkOperation.Unary(ZkOperation.UnaryOp.ARITH_NEGATE_SIGNED, sub));
    circuit.setRoots(List.of(root));
    assertInvalidExpectedTypes(
        circuit,
        List.of(),
        "Circuit : Inconsistency between number of roots (0), and expected output types (1)");
  }

  @Test
  void testWrongExpectedType() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    // Sub-gate has wrong output type!
    final ZkAddress root = circuit.add(ZkType.SBOOL, ZkOperation.Constant.fromInt(ZkType.SBI32, 1));
    circuit.setRoots(List.of(root));
    assertInvalid(
        circuit,
        List.of(ZkType.SBOOL),
        "Gate 0  : Expected type SBI1, but operation produced type SBI32");
  }

  @Test
  void testInvalidOutputType() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    // Sub-gate has wrong output type!
    final ZkAddress sub = circuit.add(ZkType.SBOOL, ZkOperation.Constant.fromInt(ZkType.SBOOL, 1));
    final ZkAddress root =
        circuit.add(
            ZkType.SBOOL, new ZkOperation.Unary(ZkOperation.UnaryOp.ARITH_NEGATE_SIGNED, sub));
    circuit.setRoots(List.of(root));
    assertInvalidExpectedTypes(
        circuit, List.of(ZkType.SBI32), "Circuit : Expected type SBI32, but got type SBI1");
  }

  @Test
  void testInvalidBinaryRight() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    // Right sub-gate has wrong type
    final ZkAddress sub1 =
        circuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 17));
    final ZkAddress sub2 = circuit.add(ZkType.SBOOL, ZkOperation.Constant.fromInt(ZkType.SBOOL, 1));
    final ZkAddress root =
        circuit.add(
            ZkType.SBI32,
            new ZkOperation.Binary(ZkOperation.BinaryOp.ARITH_MULTIPLY_SIGNED, sub1, sub2));
    circuit.setRoots(List.of(root));
    assertInvalid(
        circuit, List.of(ZkType.SBI32), "Gate 2  : Expected type SBI32, but got type SBI1");
  }

  @Test
  void testInvalidTernaryFirst() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    // First sub-gate has wrong type
    final ZkAddress sub1 =
        circuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 17));
    final ZkAddress sub2 =
        circuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 17));
    final ZkAddress sub3 =
        circuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 17));
    final ZkAddress root = circuit.add(ZkType.SBI32, new ZkOperation.Select(sub1, sub2, sub3));
    circuit.setRoots(List.of(root));
    assertInvalid(
        circuit, List.of(ZkType.SBI32), "Gate 3  : Expected type SBI1, but got type SBI32");
  }

  @Test
  void testInvalidTernarySecond() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    // First sub-gate has wrong type
    final ZkAddress sub1 = circuit.add(ZkType.SBOOL, ZkOperation.Constant.fromInt(ZkType.SBOOL, 0));
    final ZkAddress sub2 = circuit.add(ZkType.SBOOL, ZkOperation.Constant.fromInt(ZkType.SBOOL, 1));
    final ZkAddress sub3 =
        circuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 17));
    final ZkAddress root = circuit.add(ZkType.SBI32, new ZkOperation.Select(sub1, sub2, sub3));
    circuit.setRoots(List.of(root));
    assertInvalid(
        circuit, List.of(ZkType.SBI32), "Gate 3  : Expected type SBI1, but got type SBI32");
  }

  @Test
  void testInvalidTernaryThird() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    // First sub-gate has wrong type
    final ZkAddress sub1 = circuit.add(ZkType.SBOOL, ZkOperation.Constant.fromInt(ZkType.SBOOL, 0));
    final ZkAddress sub2 =
        circuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 17));
    final ZkAddress sub3 = circuit.add(ZkType.SBOOL, ZkOperation.Constant.fromInt(ZkType.SBOOL, 1));
    final ZkAddress root = circuit.add(ZkType.SBI32, new ZkOperation.Select(sub1, sub2, sub3));
    circuit.setRoots(List.of(root));
    assertInvalid(
        circuit, List.of(ZkType.SBI32), "Gate 3  : Expected type SBI32, but got type SBI1");
  }

  @Test
  void testValid() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    final ZkAddress sub =
        circuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 123));
    final ZkAddress root =
        circuit.add(
            ZkType.SBI32, new ZkOperation.Unary(ZkOperation.UnaryOp.ARITH_NEGATE_SIGNED, sub));
    circuit.setRoots(List.of(root));
    assertValid(circuit, List.of(ZkType.SBI32));
  }

  @Test
  void testValidMultiOutputs() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    final ZkAddress sub1 =
        circuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 123));
    final ZkAddress root1 =
        circuit.add(
            ZkType.SBI32, new ZkOperation.Unary(ZkOperation.UnaryOp.ARITH_NEGATE_SIGNED, sub1));
    final ZkAddress sub2 = circuit.add(ZkType.SBOOL, ZkOperation.Constant.fromInt(ZkType.SBOOL, 0));
    final ZkAddress root2 =
        circuit.add(ZkType.SBOOL, new ZkOperation.Unary(ZkOperation.UnaryOp.BITWISE_NOT, sub2));
    circuit.setRoots(List.of(root1, root2));

    assertValid(circuit, List.of(ZkType.SBI32, ZkType.SBOOL));
  }

  @Test
  void testInvalidConstants() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    // They have all wrong types
    final ZkAddress sub1 =
        circuit.add(
            ZkType.SBOOL, new ZkOperation.Constant(ZkType.SBOOL, List.of(true, true, true)));
    final ZkAddress sub2 =
        circuit.add(ZkType.SBOOL, new ZkOperation.Constant(ZkType.SBOOL, List.of(false, true)));
    final ZkAddress sub3 =
        circuit.add(
            ZkType.SBOOL, new ZkOperation.Constant(ZkType.SBOOL, List.of(false, false, false)));
    final ZkAddress root = circuit.add(ZkType.SBOOL, new ZkOperation.Select(sub1, sub2, sub3));
    circuit.setRoots(List.of(root));
    assertInvalid(
        circuit,
        List.of(new ZkType.Sbi(1)),
        "Gate 0  : Constant 0b111 does not fit type SBI1",
        "Gate 1  : Constant 0b10 does not fit type SBI1",
        "Gate 2  : Constant 0b000 does not fit type SBI1");
  }

  @Test
  void testValidConstants() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    // They have all wrong types
    final ZkAddress sub1 = circuit.add(ZkType.SBOOL, ZkOperation.Constant.fromInt(ZkType.SBOOL, 1));
    final ZkAddress sub2 =
        circuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, -1));
    final ZkAddress sub3 =
        circuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 999));
    final ZkAddress root = circuit.add(ZkType.SBI32, new ZkOperation.Select(sub1, sub2, sub3));
    circuit.setRoots(List.of(root));
    assertValid(circuit, List.of(ZkType.SBI32));
  }

  @Test
  void testInvalidExtract() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    // They have all wrong types
    final ZkAddress sub1 = circuit.add(ZkType.SBOOL, ZkOperation.Constant.fromInt(ZkType.SBOOL, 0));
    final ZkAddress sub2 = circuit.add(ZkType.SBOOL, new ZkOperation.Extract(sub1, -1, -1));
    final ZkAddress sub3 = circuit.add(ZkType.SBOOL, new ZkOperation.Extract(sub1, 1, 1));
    final ZkAddress root = circuit.add(ZkType.SBOOL, new ZkOperation.Select(sub1, sub2, sub3));
    circuit.setRoots(List.of(root));

    assertInvalid(
        circuit,
        List.of(new ZkType.Sbi(0)),
        "Gate 1  : Bit offset -1 should be non-negative",
        "Gate 1  : Bit width -1 should be non-negative",
        "Gate 2  : Extracted bit range [1,2[, extends outside 1-bit input");
  }

  @Test
  void testInvalidSignExtend() {
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    final ZkAddress sub1 = circuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 0));
    final ZkAddress root =
        circuit.add(ZkType.SBOOL, new ZkOperation.SignExtend(ZkType.SBOOL, sub1));
    circuit.setRoots(List.of(root));

    assertInvalid(
        circuit,
        List.of(ZkType.SBOOL),
        "Gate 1  : Sign-extend input type SBI32 must not be larger than output type SBI1");
  }

  @Test
  void testWeirdValidExtract() {
    //
    final ZkCircuitAppendable circuit = new ZkCircuitImpl();
    final ZkAddress sub1 = circuit.add(ZkType.SBOOL, ZkOperation.Constant.fromInt(ZkType.SBOOL, 0));
    final ZkAddress sub2 = circuit.add(ZkType.SBI0, new ZkOperation.Extract(sub1, 0, 0));
    circuit.setRoots(List.of(sub2));

    //
    assertValid(circuit, List.of(new ZkType.Sbi(0)));
  }

  private static void assertValid(ZkCircuit circuit, List<ZkType> expectedOutputTypes) {
    ZkCircuitValidator.validateZkCircuit(circuit, expectedOutputTypes);
    ZkCircuitValidator.validateZkCircuit(circuit, null);
  }

  private static void assertInvalid(
      ZkCircuit circuit, List<ZkType> expectedOutputTypes, String... messages) {
    assertInvalidInternal(circuit, expectedOutputTypes, messages);
    assertInvalidInternal(circuit, null, messages);
  }

  private static void assertInvalidExpectedTypes(
      ZkCircuit circuit, List<ZkType> expectedOutputTypes, String... messages) {
    assertInvalidInternal(circuit, expectedOutputTypes, messages);
    ZkCircuitValidator.validateZkCircuit(circuit, null);
  }

  private static void assertInvalidInternal(
      ZkCircuit circuit, List<ZkType> expectedOutputTypes, String... messages) {
    final var asserter =
        Assertions.assertThatThrownBy(
                () -> ZkCircuitValidator.validateZkCircuit(circuit, expectedOutputTypes))
            .isInstanceOf(ZkCircuitValidator.ZkCircuitInvalid.class);
    for (final String msg : messages) {
      asserter.hasMessageContaining(msg);
    }
  }
}
