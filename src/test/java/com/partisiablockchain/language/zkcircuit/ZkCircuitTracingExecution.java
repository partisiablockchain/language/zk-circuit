package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Testing {@link ZkCircuitTracingExecution} implementation for testing parallel instruction
 * dispatching.
 *
 * <p>Doesn't really do anything by itself; requires a different {@link ZkCircuitExecution}
 * implementation to perform execution.
 */
public final class ZkCircuitTracingExecution<ValueT> implements ZkCircuitExecution<ValueT> {

  private final ZkCircuitExecution<ValueT> innerExecution;
  private final ArrayList<OperationTrace> traces;

  /**
   * Operation trace instance.
   *
   * @param operationName Name of the operation that have occured.
   * @param numParallelArguments Number of arguments that have been supplied in parallel.
   */
  public record OperationTrace(String operationName, int numParallelArguments) {}

  /**
   * Create new {@link ZkCircuitTracingExecution}.
   *
   * @param innerExecution Implementation to delegate to.
   */
  public ZkCircuitTracingExecution(ZkCircuitExecution<ValueT> innerExecution) {
    this.innerExecution = requireNonNull(innerExecution);
    this.traces = new ArrayList<>();
  }

  /**
   * Gets the operation trace that have occured so far.
   *
   * @return Immutable operation trace.
   */
  public List<OperationTrace> getOperationTrace() {
    return List.copyOf(traces);
  }

  @Override
  public ValueT executeGateConstant(final ZkOperation.Constant gate) {
    traces.add(new OperationTrace("constant", 1));
    return innerExecution.executeGateConstant(gate);
  }

  @Override
  public ValueT executeGateLoad(final ZkOperation.Load gate) {
    traces.add(new OperationTrace("load", 1));
    return innerExecution.executeGateLoad(gate);
  }

  @Override
  public ValueT executeGateSignExtend(final ZkOperation.SignExtend gate, final ValueT input1) {
    traces.add(new OperationTrace("sign_extend", 1));
    return innerExecution.executeGateSignExtend(gate, input1);
  }

  @Override
  public ValueT executeGateUnary(final ZkOperation.Unary gate, final ValueT input1) {
    traces.add(new OperationTrace(gate.operation().toString().toLowerCase(Locale.ROOT), 1));
    return innerExecution.executeGateUnary(gate, input1);
  }

  @Override
  public List<ValueT> executeGatesBinary(
      final ZkOperation.BinaryOp binop, final List<ValueT> inputs1, final List<ValueT> inputs2) {
    traces.add(new OperationTrace(binop.toString().toLowerCase(Locale.ROOT), inputs1.size()));
    return innerExecution.executeGatesBinary(binop, inputs1, inputs2);
  }

  @Override
  public ValueT executeGateSelect(
      final ZkOperation.Select gate,
      final ValueT inputCond,
      final ValueT inputThen,
      final ValueT inputElse) {
    traces.add(new OperationTrace("select", 1));
    return innerExecution.executeGateSelect(gate, inputCond, inputThen, inputElse);
  }

  @Override
  public ValueT executeGateExtract(final ZkOperation.Extract gate, final ValueT value) {
    traces.add(new OperationTrace("extract", 1));
    return innerExecution.executeGateExtract(gate, value);
  }
}
