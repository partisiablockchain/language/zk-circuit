package com.partisiablockchain.language.zkcircuit.testutil;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcircuit.ZkCircuitExecution;
import com.partisiablockchain.language.zkcircuit.ZkOperation;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test execution of circuits. */
public final class ZkCircuitExecutionComplianceTestingTest {

  @Test
  void checkFailureModes() {
    final List<String> complianceReport =
        ZkCircuitExecutionComplianceTesting.testCompliance(new BadExecution(), Integer::equals);
    Assertions.assertThat(complianceReport)
        .containsExactlyInAnyOrder(
            "ZkOperation.Constant must not produce same value for 0 and 2",
            "ZkOperation.UnaryOp.UNKNOWN must throw UnsupportedOperationException",
            "ZkOperation.BinaryOp.UNKNOWN must throw UnsupportedOperationException",
            "ZkOperation.SignExtend must convert 0 to 31, not to 0",
            "ZkOperation.SignExtend must convert 1 to 32, not to 1");
  }

  private record BadExecution() implements ZkCircuitExecution<Integer> {

    @Override
    public Integer executeGateConstant(final ZkOperation.Constant gate) {
      // Weird implementation of constant values:
      return gate.constantBits().size() - 1 + (gate.constantBits().get(0) ? 1 : 0);
    }

    @Override
    public Integer executeGateLoad(final ZkOperation.Load gate) {
      throw new UnsupportedOperationException();
    }

    @Override
    public Integer executeGateSignExtend(final ZkOperation.SignExtend gate, final Integer input1) {
      return input1;
    }

    @Override
    public Integer executeGateUnary(final ZkOperation.Unary gate, final Integer input1) {
      return input1;
    }

    @Override
    public List<Integer> executeGatesBinary(
        final ZkOperation.BinaryOp gate, final List<Integer> inputs1, final List<Integer> inputs2) {
      return inputs1;
    }

    @Override
    public Integer executeGateSelect(
        final ZkOperation.Select gate,
        final Integer inputCond,
        final Integer inputThen,
        final Integer inputElse) {
      return inputCond;
    }

    @Override
    public Integer executeGateExtract(final ZkOperation.Extract gate, final Integer value) {
      return value;
    }
  }
}
