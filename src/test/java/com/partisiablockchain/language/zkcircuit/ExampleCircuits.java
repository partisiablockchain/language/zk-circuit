package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CheckReturnValue;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

final class ExampleCircuits {

  private ExampleCircuits() {}

  record TestCase(
      String testname,
      ZkCircuit circuit,
      ZkType inputType,
      List<Integer> inputs,
      List<ZkType> expectedOutputTypes,
      List<Integer> expectedOutput) {

    TestCaseForBad asBad(String expectedErrorMessage) {
      return new TestCaseForBad(testname, circuit, inputType, inputs, expectedErrorMessage);
    }

    TestCaseForBad asBad(String expectedErrorMessage, List<Integer> inputs) {
      return new TestCaseForBad(testname, circuit, inputType, inputs, expectedErrorMessage);
    }
  }

  record TestCaseForBad(
      String testname,
      ZkCircuit circuit,
      ZkType inputType,
      List<Integer> inputs,
      String expectedErrorMessage) {}

  private static ZkAddress addConstant(ZkCircuitAppendable zkCircuit, int bitwidth, int constant) {
    return zkCircuit.add(
        new ZkType.Sbi(bitwidth), ZkOperation.Constant.fromInt(new ZkType.Sbi(bitwidth), constant));
  }

  private static ZkAddress addConcat(
      ZkCircuitAppendable zkCircuit, int bitwidth, ZkAddress left, ZkAddress right) {
    return zkCircuit.add(
        new ZkType.Sbi(bitwidth),
        new ZkOperation.Binary(ZkOperation.BinaryOp.BIT_CONCAT, left, right));
  }

  @CheckReturnValue
  static List<TestCase> testCases() {
    final List<TestCase> cases = new ArrayList<>();

    for (int c = 1; c < 100; c += 7) {
      final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
      final ZkAddress constValue = addConstant(zkCircuit, 32, c);
      zkCircuit.setRoots(List.of(constValue));
      cases.add(
          new TestCase(
              "constant_sbi32_" + c,
              zkCircuit,
              ZkType.SBOOL,
              List.of(),
              List.of(ZkType.SBI32),
              List.of(c)));
    }

    for (int c = 3; c < 100; c += 7) {
      cases.add(unopCase(ZkOperation.UnaryOp.ARITH_NEGATE_SIGNED, ZkType.SBI32, c, -c));
    }

    for (int c = 0; c <= 1; c++) {
      final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
      final ZkAddress constValue = addConstant(zkCircuit, 1, c);
      zkCircuit.setRoots(List.of(constValue));

      cases.add(
          new TestCase(
              "constant_sbi1_" + c,
              zkCircuit,
              ZkType.SBOOL,
              List.of(),
              List.of(ZkType.SBOOL),
              List.of(c)));
    }

    for (int c = 0; c <= 1; c++) {
      cases.add(unopCase(ZkOperation.UnaryOp.BITWISE_NOT, ZkType.SBOOL, c, 1 - c));
    }

    for (int c = 0; c < 1; c++) {
      cases.add(unopCase(ZkOperation.UnaryOp.BITWISE_NOT, ZkType.SBI32, 0x01020304, 0xfefdfcfb));
    }

    for (int c1 = 0; c1 < 16; c1++) {
      for (int c2 = 0; c2 < 16; c2++) {
        cases.add(
            binopCase(
                ZkOperation.BinaryOp.BITWISE_OR,
                new ZkType.Sbi(4),
                c1,
                c2,
                c1 | c2,
                new ZkType.Sbi(4)));
        cases.add(
            binopCase(
                ZkOperation.BinaryOp.BITWISE_AND,
                new ZkType.Sbi(4),
                c1,
                c2,
                c1 & c2,
                new ZkType.Sbi(4)));
        cases.add(
            binopCase(
                ZkOperation.BinaryOp.ARITH_MULTIPLY_SIGNED,
                new ZkType.Sbi(8),
                c1,
                c2,
                c1 * c2,
                new ZkType.Sbi(8)));
        cases.add(
            binopCase(
                ZkOperation.BinaryOp.ARITH_ADD_WRAPPING,
                new ZkType.Sbi(8),
                c1,
                c2,
                c1 + c2,
                new ZkType.Sbi(8)));
        cases.add(
            binopCase(
                ZkOperation.BinaryOp.ARITH_SUBTRACT_WRAPPING,
                new ZkType.Sbi(8),
                c1,
                c2,
                (256 + c1 - c2) & 0xff,
                new ZkType.Sbi(8)));
        cases.add(
            binopCase(
                ZkOperation.BinaryOp.BIT_CONCAT,
                new ZkType.Sbi(4),
                c1,
                c2,
                (c1 << 4) | c2,
                new ZkType.Sbi(8)));
        cases.add(
            binopCase(
                ZkOperation.BinaryOp.CMP_EQUALS,
                new ZkType.Sbi(4),
                c1,
                c2,
                c1 == c2 ? 1 : 0,
                ZkType.SBOOL));
        cases.add(
            binopCase(
                ZkOperation.BinaryOp.CMP_LESS_THAN_SIGNED,
                new ZkType.Sbi(5),
                c1 - 8,
                c2 - 8,
                c1 < c2 ? 1 : 0,
                ZkType.SBOOL));
        cases.add(
            binopCase(
                ZkOperation.BinaryOp.CMP_LESS_THAN_OR_EQUALS_SIGNED,
                new ZkType.Sbi(5),
                c1 - 8,
                c2 - 8,
                c1 <= c2 ? 1 : 0,
                ZkType.SBOOL));
        cases.add(
            binopCase(
                ZkOperation.BinaryOp.CMP_LESS_THAN_UNSIGNED,
                new ZkType.Sbi(4),
                c1,
                c2,
                c1 < c2 ? 1 : 0,
                ZkType.SBOOL));
        cases.add(
            binopCase(
                ZkOperation.BinaryOp.CMP_LESS_THAN_OR_EQUALS_UNSIGNED,
                new ZkType.Sbi(4),
                c1,
                c2,
                c1 <= c2 ? 1 : 0,
                ZkType.SBOOL));
      }
    }

    for (int c2 = 0; c2 <= 0xf; c2++) {
      final ZkCircuitImpl zkCircuit = new ZkCircuitImpl();
      final ZkAddress input1Addr = addConstant(zkCircuit, 4, c2);
      final ZkAddress v1 = addConcat(zkCircuit, 8, input1Addr, input1Addr);
      final ZkAddress v2 = addConcat(zkCircuit, 16, v1, v1);
      zkCircuit.setRoots(List.of(v2));

      final int expected = c2 | (c2 << 4) | (c2 << 8) | (c2 << 12);
      cases.add(
          new TestCase(
              "concat_x4",
              zkCircuit,
              ZkType.SBOOL,
              List.of(),
              List.of(new ZkType.Sbi(16)),
              List.of(expected)));
    }

    for (int c2 = 0; c2 <= 0xf; c2++) {
      final ZkCircuitImpl zkCircuit = new ZkCircuitImpl();
      final ZkAddress input1Addr = addConstant(zkCircuit, 4, c2);
      final ZkAddress v1 = addConcat(zkCircuit, 8, input1Addr, input1Addr);
      final ZkAddress v2 = addConcat(zkCircuit, 12, v1, input1Addr);
      zkCircuit.setRoots(List.of(v2));

      final int expected = c2 | (c2 << 4) | (c2 << 8);
      cases.add(
          new TestCase(
              "concat_x3",
              zkCircuit,
              ZkType.SBOOL,
              List.of(),
              List.of(new ZkType.Sbi(12)),
              List.of(expected)));
    }

    for (int c2 = 0; c2 <= 9; c2++) {
      final ZkCircuitImpl zkCircuit = new ZkCircuitImpl();
      final ZkAddress input1Addr =
          zkCircuit.add(
              new ZkType.Sbi(8), ZkOperation.Constant.fromInt(new ZkType.Sbi(8), 0x40 | c2));
      final ZkAddress computedValueAddr =
          zkCircuit.add(new ZkType.Sbi(4), new ZkOperation.Extract(input1Addr, 4, 4));
      zkCircuit.setRoots(List.of(computedValueAddr));
      cases.add(
          new TestCase(
              "extract_4_" + c2,
              zkCircuit,
              ZkType.SBOOL,
              List.of(),
              List.of(new ZkType.Sbi(4)),
              List.of(4)));
    }

    for (int c2 = 0; c2 <= 9; c2++) {
      final ZkCircuitImpl zkCircuit = new ZkCircuitImpl();
      final ZkAddress input1Addr =
          zkCircuit.add(
              new ZkType.Sbi(8), ZkOperation.Constant.fromInt(new ZkType.Sbi(8), 0x40 | c2));
      final ZkAddress computedValueAddr =
          zkCircuit.add(new ZkType.Sbi(4), new ZkOperation.Extract(input1Addr, 4, 0));
      zkCircuit.setRoots(List.of(computedValueAddr));
      cases.add(
          new TestCase(
              "extract_0_" + c2,
              zkCircuit,
              ZkType.SBOOL,
              List.of(),
              List.of(new ZkType.Sbi(4)),
              List.of(c2)));
    }

    for (int c = 0; c <= 1; c++) {
      final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
      final ZkAddress constValue = addConstant(zkCircuit, 1, c);
      final ZkAddress v1 = addConstant(zkCircuit, 32, 231);
      final ZkAddress v2 = addConstant(zkCircuit, 32, 666);
      final ZkAddress negatedValue =
          zkCircuit.add(ZkType.SBI32, new ZkOperation.Select(constValue, v1, v2));
      zkCircuit.setRoots(List.of(negatedValue));
      cases.add(
          new TestCase(
              "select_" + c,
              zkCircuit,
              ZkType.SBOOL,
              List.of(),
              List.of(ZkType.SBI32),
              List.of(c == 0 ? 666 : 231)));
    }

    signExtendTestCases(cases);

    cases.add(testCaseForParallelDispatching());
    cases.add(testCaseCmpSelect());

    return cases;
  }

  private static void signExtendTestCases(List<TestCase> cases) {
    for (int fromSize = 1; fromSize < 10; fromSize++) {
      for (int toSize = fromSize; toSize < fromSize * 2; toSize++) {
        final ZkType.Sbi toType = new ZkType.Sbi(toSize);

        final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
        final ZkAddress constValue = addConstant(zkCircuit, fromSize, 0);
        final ZkAddress signExtendedValue =
            zkCircuit.add(toType, new ZkOperation.SignExtend(toType, constValue));
        zkCircuit.setRoots(List.of(signExtendedValue));

        cases.add(
            new TestCase(
                "sign_extend_%s_to_%s".formatted(fromSize, toSize),
                zkCircuit,
                toType,
                List.of(),
                List.of(toType),
                List.of(0)));
      }
    }
  }

  static TestCase testCaseForParallelDispatching() {
    final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();

    final ZkType type1 = ZkType.SBI32;
    final ZkType type2 = new ZkType.Sbi(16);

    final ZkAddress a1 =
        zkCircuit.add(type1, new ZkOperation.Load(type1, new ZkOperation.ZkVariableId(1)));
    final ZkAddress a2 =
        zkCircuit.add(type1, new ZkOperation.Load(type1, new ZkOperation.ZkVariableId(2)));
    final ZkAddress a3 =
        zkCircuit.add(type1, new ZkOperation.Load(type1, new ZkOperation.ZkVariableId(3)));
    final ZkAddress a1S =
        zkCircuit.add(type2, new ZkOperation.Load(type2, new ZkOperation.ZkVariableId(1)));
    final ZkAddress a2S =
        zkCircuit.add(type2, new ZkOperation.Load(type2, new ZkOperation.ZkVariableId(2)));
    final ZkAddress a3S =
        zkCircuit.add(type2, new ZkOperation.Load(type2, new ZkOperation.ZkVariableId(3)));

    final ZkAddress a4 =
        zkCircuit.add(
            type1, new ZkOperation.Binary(ZkOperation.BinaryOp.ARITH_ADD_WRAPPING, a1, a2));
    final ZkAddress a5 =
        zkCircuit.add(
            type1, new ZkOperation.Binary(ZkOperation.BinaryOp.ARITH_ADD_WRAPPING, a1, a3));
    final ZkAddress a6 =
        zkCircuit.add(type1, new ZkOperation.Binary(ZkOperation.BinaryOp.BITWISE_AND, a2, a3));

    final ZkAddress a7 =
        zkCircuit.add(type1, new ZkOperation.Binary(ZkOperation.BinaryOp.BITWISE_AND, a4, a5));
    final ZkAddress a8 =
        zkCircuit.add(type1, new ZkOperation.Binary(ZkOperation.BinaryOp.BITWISE_AND, a5, a6));
    final ZkAddress a9 =
        zkCircuit.add(type1, new ZkOperation.Binary(ZkOperation.BinaryOp.BITWISE_AND, a7, a8));

    final ZkAddress a10 =
        zkCircuit.add(type2, new ZkOperation.Binary(ZkOperation.BinaryOp.BITWISE_AND, a1S, a2S));
    final ZkAddress a11 =
        zkCircuit.add(type2, new ZkOperation.Binary(ZkOperation.BinaryOp.BITWISE_AND, a1S, a3S));
    final ZkAddress a12 =
        zkCircuit.add(type2, new ZkOperation.Binary(ZkOperation.BinaryOp.BITWISE_AND, a2S, a3S));
    final ZkAddress a13 =
        zkCircuit.add(type2, new ZkOperation.Binary(ZkOperation.BinaryOp.BITWISE_AND, a11, a12));
    final ZkAddress a14 =
        zkCircuit.add(type2, new ZkOperation.Binary(ZkOperation.BinaryOp.BITWISE_AND, a13, a10));

    zkCircuit.setRoots(List.of(a9, a14));

    return new TestCase(
        "testCaseForParallelDispatching",
        zkCircuit,
        type1,
        List.of(0xFF, 0x87, 0x34),
        List.of(type1, type2),
        List.of(0, 4));
  }

  static TestCase testCaseCmpSelect() {
    final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();

    final ZkType type1 = ZkType.SBI32;

    final ZkAddress a1 =
        zkCircuit.add(type1, new ZkOperation.Load(type1, new ZkOperation.ZkVariableId(1)));
    final ZkAddress a2 =
        zkCircuit.add(type1, new ZkOperation.Load(type1, new ZkOperation.ZkVariableId(2)));
    final ZkAddress a3 =
        zkCircuit.add(type1, new ZkOperation.Load(type1, new ZkOperation.ZkVariableId(3)));

    final ZkAddress a4 =
        zkCircuit.add(
            ZkType.SBOOL, new ZkOperation.Binary(ZkOperation.BinaryOp.CMP_EQUALS, a1, a2));

    final ZkAddress a5 = zkCircuit.add(type1, new ZkOperation.Select(a4, a2, a3));

    zkCircuit.setRoots(List.of(a5));

    return new TestCase(
        "testCaseCmpSelect",
        zkCircuit,
        type1,
        List.of(0xFF, 0x87, 0x34),
        List.of(type1),
        List.of(0x34));
  }

  @CheckReturnValue
  static List<TestCaseForBad> testCasesForBad() {
    final List<TestCaseForBad> cases = new ArrayList<>();

    cases.add(
        binopCase(ZkOperation.BinaryOp.UNKNOWN, new ZkType.Sbi(8), 0, 0, 0, new ZkType.Sbi(8))
            .asBad("Unknown binop: UNKNOWN"));

    cases.add(
        unopCase(ZkOperation.UnaryOp.UNKNOWN, new ZkType.Sbi(8), 0, 0)
            .asBad("Unknown unop: UNKNOWN"));

    return cases;
  }

  @CheckReturnValue
  private static TestCase binopCase(
      final ZkOperation.BinaryOp operation,
      final ZkType.Sbi inputType,
      final int input1,
      final int input2,
      final int output,
      final ZkType.Sbi outputType) {
    // Produce circuit
    final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
    final ZkAddress input1Addr =
        zkCircuit.add(inputType, new ZkOperation.Load(inputType, new ZkOperation.ZkVariableId(1)));
    final ZkAddress input2Addr =
        zkCircuit.add(inputType, ZkOperation.Constant.fromInt(inputType, input2));
    final ZkAddress computedValueAddr =
        zkCircuit.add(outputType, new ZkOperation.Binary(operation, input1Addr, input2Addr));
    zkCircuit.setRoots(List.of(computedValueAddr));

    // Produce name
    final String name =
        "%s_sbi%s_%d_%d"
            .formatted(
                operation.toString().toLowerCase(Locale.ROOT),
                inputType.bitwidth(),
                input1,
                input2);
    return new TestCase(
        name, zkCircuit, inputType, List.of(input1), List.of(outputType), List.of(output));
  }

  @CheckReturnValue
  private static TestCase unopCase(
      final ZkOperation.UnaryOp operation,
      final ZkType.Sbi inputType,
      final int input1,
      final int output) {
    // Produce circuit
    final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
    final ZkAddress input1Addr =
        zkCircuit.add(inputType, new ZkOperation.Load(inputType, new ZkOperation.ZkVariableId(1)));
    final ZkAddress computedValueAddr =
        zkCircuit.add(inputType, new ZkOperation.Unary(operation, input1Addr));
    zkCircuit.setRoots(List.of(computedValueAddr));

    // Produce name
    final String name =
        "%s_sbi%s_%d"
            .formatted(operation.toString().toLowerCase(Locale.ROOT), inputType.bitwidth(), input1);
    return new TestCase(
        name, zkCircuit, inputType, List.of(input1), List.of(inputType), List.of(output));
  }
}
