package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Unit test of {@link ZkOperation}. */
public final class ZkOperationTest {

  /** Normalization can flip arguments for commutative operations. */
  @Test
  public void normalizedCanFlipArguments() {
    final ZkAddress addr1 = new ZkAddress(1);
    final ZkAddress addr2 = new ZkAddress(2);
    final ZkOperation.Binary binop1 =
        ZkOperation.Binary.newNormalized(ZkOperation.BinaryOp.BITWISE_AND, addr1, addr2);
    final ZkOperation.Binary binop2 =
        ZkOperation.Binary.newNormalized(ZkOperation.BinaryOp.BITWISE_AND, addr2, addr1);

    Assertions.assertThat(binop1.addr1()).isEqualTo(addr1);
    Assertions.assertThat(binop1.addr2()).isEqualTo(addr2);
    Assertions.assertThat(binop1).isEqualTo(binop2);
  }

  @Test
  public void normalizedCanFlipArgumentsWhenEqual() {
    final ZkAddress addr1 = new ZkAddress(1);
    final ZkAddress addr2 = new ZkAddress(1);
    final ZkOperation.Binary binop2 =
        ZkOperation.Binary.newNormalized(ZkOperation.BinaryOp.BITWISE_AND, addr2, addr1);

    Assertions.assertThat(binop2.addr1()).isEqualTo(addr1).isNotSameAs(addr1);
    Assertions.assertThat(binop2.addr2()).isEqualTo(addr2).isNotSameAs(addr2);
  }

  /** Normalization will not flip arguments for non-commutative operations. */
  @Test
  public void normalizedWontFlipArgumentsForConcat() {
    final ZkAddress addr1 = new ZkAddress(1);
    final ZkAddress addr2 = new ZkAddress(2);
    final ZkOperation.Binary binop1 =
        ZkOperation.Binary.newNormalized(ZkOperation.BinaryOp.BIT_CONCAT, addr1, addr2);
    final ZkOperation.Binary binop2 =
        ZkOperation.Binary.newNormalized(ZkOperation.BinaryOp.BIT_CONCAT, addr2, addr1);

    Assertions.assertThat(binop1).isNotEqualTo(binop2);
  }

  /** Operations are marked as either associative or not. */
  @Test
  public void isAssociative() {
    Assertions.assertThat(ZkOperation.BinaryOp.CMP_EQUALS.isAssociative()).isTrue();
    Assertions.assertThat(ZkOperation.BinaryOp.BIT_CONCAT.isAssociative()).isFalse();
    Assertions.assertThat(ZkOperation.BinaryOp.UNKNOWN.isAssociative()).isFalse();
  }
}
