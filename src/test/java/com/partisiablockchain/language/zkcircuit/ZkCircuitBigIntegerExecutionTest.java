package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcircuit.testutil.ZkCircuitExecutionComplianceTesting;
import java.math.BigInteger;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test execution of circuits. */
public final class ZkCircuitBigIntegerExecutionTest {

  @Test
  void valueConstructor() {
    Assertions.assertThatCode(
            () -> new ZkCircuitBigIntegerExecution.Value(BigInteger.valueOf(-1), 99))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Value -1 should have been non-negative");
    Assertions.assertThatCode(
            () -> new ZkCircuitBigIntegerExecution.Value(BigInteger.valueOf(3213), 1))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Value 3213 cannot fit into 1 bits");
  }

  @Test
  void toSigned1Bit() {
    Assertions.assertThat(ZkCircuitBigIntegerExecution.toSigned(BigInteger.valueOf(0), 1))
        .isEqualTo(0);
    Assertions.assertThat(ZkCircuitBigIntegerExecution.toSigned(BigInteger.valueOf(1), 1))
        .isEqualTo(-1);
  }

  @Test
  void toSigned2Bit() {
    Assertions.assertThat(ZkCircuitBigIntegerExecution.toSigned(BigInteger.valueOf(0), 2))
        .isEqualTo(0);
    Assertions.assertThat(ZkCircuitBigIntegerExecution.toSigned(BigInteger.valueOf(1), 2))
        .isEqualTo(1);
    Assertions.assertThat(ZkCircuitBigIntegerExecution.toSigned(BigInteger.valueOf(2), 2))
        .isEqualTo(-2);
    Assertions.assertThat(ZkCircuitBigIntegerExecution.toSigned(BigInteger.valueOf(3), 2))
        .isEqualTo(-1);
  }

  @Test
  public void allocateByteArrayFromBitsTest() {
    Assertions.assertThat(ZkCircuitBigIntegerExecution.allocateByteArrayFromBits(0)).hasSize(0);
    Assertions.assertThat(ZkCircuitBigIntegerExecution.allocateByteArrayFromBits(1)).hasSize(1);
    Assertions.assertThat(ZkCircuitBigIntegerExecution.allocateByteArrayFromBits(2)).hasSize(1);
    Assertions.assertThat(ZkCircuitBigIntegerExecution.allocateByteArrayFromBits(3)).hasSize(1);
    Assertions.assertThat(ZkCircuitBigIntegerExecution.allocateByteArrayFromBits(4)).hasSize(1);
    Assertions.assertThat(ZkCircuitBigIntegerExecution.allocateByteArrayFromBits(5)).hasSize(1);
    Assertions.assertThat(ZkCircuitBigIntegerExecution.allocateByteArrayFromBits(6)).hasSize(1);
    Assertions.assertThat(ZkCircuitBigIntegerExecution.allocateByteArrayFromBits(7)).hasSize(1);
    Assertions.assertThat(ZkCircuitBigIntegerExecution.allocateByteArrayFromBits(8)).hasSize(1);
    Assertions.assertThat(ZkCircuitBigIntegerExecution.allocateByteArrayFromBits(9)).hasSize(2);
    Assertions.assertThat(ZkCircuitBigIntegerExecution.allocateByteArrayFromBits(16)).hasSize(2);
    Assertions.assertThat(ZkCircuitBigIntegerExecution.allocateByteArrayFromBits(17)).hasSize(3);
  }

  @Test
  void checkCompliance() {
    Assertions.assertThat(
            ZkCircuitExecutionComplianceTesting.testCompliance(
                new ZkCircuitBigIntegerExecution(Map.of()), (a, b) -> a.equals(b)))
        .isEmpty();
  }
}
