package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/** Test execution of circuits. */
public final class ZkCircuitExecuteTest {

  static List<ExampleCircuits.TestCase> testCases() {
    return ExampleCircuits.testCases();
  }

  static List<ExampleCircuits.TestCaseForBad> testCasesForBad() {
    return ExampleCircuits.testCasesForBad();
  }

  @ParameterizedTest
  @MethodSource("testCases")
  void executePublicBigInteger(ExampleCircuits.TestCase testCase) {
    // Create engine
    final var engine =
        new ZkCircuitEngine<>(
            new ZkCircuitBigIntegerExecution(publicInputEnv(testCase.inputs())),
            testCase.circuit());

    // Check success
    final List<Integer> results =
        engine.executeZkCircuit().stream().map(r -> r.value().intValue()).toList();
    Assertions.assertThat(results)
        .as(testCase.testname())
        .containsExactlyElementsOf(testCase.expectedOutput());
  }

  @ParameterizedTest
  @MethodSource("testCasesForBad")
  void failInExecutePublicBigInteger(ExampleCircuits.TestCaseForBad testCase) {
    // Create engine
    final var engine =
        new ZkCircuitEngine<>(
            new ZkCircuitBigIntegerExecution(publicInputEnv(testCase.inputs())),
            testCase.circuit());

    // Check fails
    Assertions.assertThatCode(() -> engine.executeZkCircuit().get(0))
        .as(testCase.testname())
        .isInstanceOf(RuntimeException.class)
        .hasMessage(testCase.expectedErrorMessage());
  }

  private static Map<Integer, BigInteger> publicInputEnv(List<Integer> inputs) {
    final Map<Integer, BigInteger> env = new HashMap<>();

    for (int idx = 0; idx < inputs.size(); idx++) {
      env.put(idx + 1, BigInteger.valueOf(inputs.get(idx)));
    }

    return Map.copyOf(env);
  }

  @Test
  void testMultiOutputCircuit() {
    final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
    final ZkAddress constValue =
        zkCircuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 101791));
    final ZkAddress addValue =
        zkCircuit.add(
            ZkType.SBI32,
            ZkOperation.Binary.newNormalized(
                ZkOperation.BinaryOp.ARITH_ADD_WRAPPING, constValue, constValue));
    final ZkAddress negatedValue =
        zkCircuit.add(
            ZkType.SBI32,
            new ZkOperation.Unary(ZkOperation.UnaryOp.ARITH_NEGATE_SIGNED, constValue));
    zkCircuit.setRoots(List.of(addValue, negatedValue));

    final var engine =
        new ZkCircuitEngine<>(
            new ZkCircuitBigIntegerExecution(publicInputEnv(List.of())), zkCircuit);

    final List<ZkCircuitBigIntegerExecution.Value> result = engine.executeZkCircuit();
    Assertions.assertThat(result.get(0).value()).isEqualTo(101791 + 101791);
    Assertions.assertThat(result.get(1).value()).isEqualTo((2L << 31) - 101791L);
  }

  @Test
  void testMultiOutputTypeCircuit() {
    final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
    final ZkAddress constValue =
        zkCircuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 101791));
    final ZkAddress addValue =
        zkCircuit.add(
            ZkType.SBI32,
            ZkOperation.Binary.newNormalized(
                ZkOperation.BinaryOp.ARITH_ADD_WRAPPING, constValue, constValue));
    final ZkAddress ltValue =
        zkCircuit.add(
            ZkType.SBI32,
            ZkOperation.Binary.newNormalized(
                ZkOperation.BinaryOp.CMP_LESS_THAN_SIGNED, constValue, addValue));
    zkCircuit.setRoots(List.of(addValue, ltValue));

    final var engine =
        new ZkCircuitEngine<>(
            new ZkCircuitBigIntegerExecution(publicInputEnv(List.of())), zkCircuit);

    final List<ZkCircuitBigIntegerExecution.Value> result = engine.executeZkCircuit();

    Assertions.assertThat(result.get(0).value()).isEqualTo(101791 + 101791);
    Assertions.assertThat(result.get(1).value()).isEqualTo(1);
  }

  @Test
  void testParallelDispatching() {
    ExampleCircuits.TestCase testCase = ExampleCircuits.testCaseForParallelDispatching();

    // Create engine
    final var tracingExecution =
        new ZkCircuitTracingExecution<>(
            new ZkCircuitBigIntegerExecution(publicInputEnv(testCase.inputs())));

    final var engine = new ZkCircuitEngine<>(tracingExecution, testCase.circuit());

    // Check success
    final List<Integer> results =
        engine.executeZkCircuit().stream().map(r -> r.value().intValue()).toList();
    Assertions.assertThat(results)
        .as(testCase.testname())
        .containsExactlyElementsOf(testCase.expectedOutput());

    Assertions.assertThat(tracingExecution.getOperationTrace())
        .as(testCase.testname())
        .containsExactly(
            new ZkCircuitTracingExecution.OperationTrace("load", 1),
            new ZkCircuitTracingExecution.OperationTrace("load", 1),
            new ZkCircuitTracingExecution.OperationTrace("load", 1),
            new ZkCircuitTracingExecution.OperationTrace("load", 1),
            new ZkCircuitTracingExecution.OperationTrace("load", 1),
            new ZkCircuitTracingExecution.OperationTrace("load", 1),
            new ZkCircuitTracingExecution.OperationTrace("arith_add_wrapping", 2),
            new ZkCircuitTracingExecution.OperationTrace("bitwise_and", 2),
            new ZkCircuitTracingExecution.OperationTrace("bitwise_and", 1),
            new ZkCircuitTracingExecution.OperationTrace("bitwise_and", 1),
            new ZkCircuitTracingExecution.OperationTrace("bitwise_and", 3),
            new ZkCircuitTracingExecution.OperationTrace("bitwise_and", 1),
            new ZkCircuitTracingExecution.OperationTrace("bitwise_and", 1));
  }
}
