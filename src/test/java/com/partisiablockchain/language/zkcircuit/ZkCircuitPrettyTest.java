package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/** Test execution of circuits. */
public final class ZkCircuitPrettyTest {

  @Test
  void testSbi32Constant() {
    final ZkCircuitAppendable zkCircuit = new ZkCircuitImpl();
    final ZkAddress a0 =
        zkCircuit.add(ZkType.SBI32, ZkOperation.Constant.fromInt(ZkType.SBI32, 99));
    final ZkAddress a1 =
        zkCircuit.add(
            ZkType.SBI32, new ZkOperation.Load(ZkType.SBI32, new ZkOperation.ZkVariableId(11)));
    final ZkAddress a2 =
        zkCircuit.add(ZkType.SBI32, new ZkOperation.Unary(ZkOperation.UnaryOp.BITWISE_NOT, a0));
    final ZkAddress a3 =
        zkCircuit.add(
            ZkType.SBOOL, new ZkOperation.Binary(ZkOperation.BinaryOp.CMP_EQUALS, a0, a2));
    final ZkAddress a4 = zkCircuit.add(ZkType.SBI32, new ZkOperation.Select(a3, a0, a1));
    final ZkAddress a5 = zkCircuit.add(ZkType.SBOOL, new ZkOperation.Extract(a4, 1, 1));
    final ZkAddress a6 = zkCircuit.add(ZkType.SBI32, new ZkOperation.SignExtend(ZkType.SBI32, a5));
    zkCircuit.setRoots(List.of(a6, a2));

    final String expectedPretty =
        """
        (zkcircuit
          (sbi32 $0 (constant sbi32 0b00000000000000000000000001100011))
          (sbi32 $1 (load sbi32 %11))
          (sbi32 $2 (bitwise_not $0))
          (sbi1 $3 (cmp_equals $0 $2))
          (sbi32 $4 (select $3 $0 $1))
          (sbi1 $5 (extract $4 1 1))
          (sbi32 $6 (sign_extend sbi32 $5))
          (output $6 $2))
        """;

    Assertions.assertThat(ZkCircuitPretty.prettyCircuit(zkCircuit))
        .isEqualTo(expectedPretty.strip());
  }

  @ParameterizedTest
  @MethodSource("testCases")
  void prettyTestCasess(ExampleCircuits.TestCase testCase) {
    Assertions.assertThat(ZkCircuitPretty.prettyCircuit(testCase.circuit()))
        .isNotNull()
        .startsWith("(zkcircuit\n");
  }

  static List<ExampleCircuits.TestCase> testCases() {
    return ExampleCircuits.testCases();
  }
}
