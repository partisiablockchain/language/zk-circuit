package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;
import java.util.stream.Stream;

/** A zero-knowledge circuit capable of performing a zero-knowledge computation. */
public interface ZkCircuit {

  /**
   * Get the ZK gate at a specific address in the circuit.
   *
   * @param address The address of the gate. Addresses are 31bit unsigned.
   * @return The gate at the address. Possibly null.
   */
  ZkCircuitGate getGate(final ZkAddress address);

  /**
   * Returns a stream of gate addresses in executable order.
   *
   * @return Stream of gate addresses. Never null.
   */
  Stream<ZkAddress> gateAddresses();

  /**
   * Get the addresses of the root gates of the zk circuit. The root gates represents the top-level
   * results being computed by the circuit.
   *
   * @return The addresses of the root zk gates. Never null.
   */
  List<ZkAddress> getRoots();
}
