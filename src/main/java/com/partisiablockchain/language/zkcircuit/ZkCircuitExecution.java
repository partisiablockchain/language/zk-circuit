package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;

/**
 * Handles manipulation of values in the circuit, whereas {@link ZkCircuitExecution} simply handles
 * the control flow.
 *
 * @param <T> Type of values for this execution.
 */
public interface ZkCircuitExecution<T> {

  /**
   * Performs execution of a constant gate.
   *
   * @param gate Gate itself.
   * @return Result variable. Must never be null.
   */
  T executeGateConstant(ZkOperation.Constant gate);

  /**
   * Performs execution of a load variable gate.
   *
   * @param gate Gate itself.
   * @return Result variable. Must never be null.
   */
  T executeGateLoad(ZkOperation.Load gate);

  /**
   * Performs execution of a unary operation gate.
   *
   * @param gate Gate itself.
   * @param input1 First input to operation.
   * @return Result variable. Must never be null.
   */
  T executeGateUnary(ZkOperation.Unary gate, T input1);

  /**
   * Performs execution of a constant gate.
   *
   * @param gate Gate itself.
   * @param inputs1 First list of inputs to operation. Must have same length as inputs2.
   * @param inputs2 Second list of inputs to operation. Must have same length as inputs1.
   * @return Result variables. Must never be null. Must have same length as inputs1.
   */
  List<T> executeGatesBinary(ZkOperation.BinaryOp gate, List<T> inputs1, List<T> inputs2);

  /**
   * Performs execution of a {@link ZkOperation.SignExtend} gate.
   *
   * @param gate Gate itself.
   * @param input1 First input to operation.
   * @return Result variable. Must never be null.
   */
  T executeGateSignExtend(ZkOperation.SignExtend gate, T input1);

  /**
   * Performs execution of a {@link ZkOperation.Extract} gate.
   *
   * @param gate Gate itself.
   * @param input1 First input to operation.
   * @return Result variable. Must never be null.
   */
  T executeGateExtract(ZkOperation.Extract gate, T input1);

  /**
   * Performs execution of a constant gate.
   *
   * @param gate Gate itself.
   * @param input1 First input to operation.
   * @param input2 Second input to operation.
   * @param input3 Third input to operation.
   * @return Result variable. Must never be null.
   */
  T executeGateSelect(ZkOperation.Select gate, T input1, T input2, T input3);
}
