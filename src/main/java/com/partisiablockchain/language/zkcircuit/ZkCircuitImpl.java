package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/** A zero-knowledge circuit capable of performing a zero-knowledge computation. */
public final class ZkCircuitImpl implements ZkCircuitAppendable {

  /** The gates of the circuit. The index in the list is the address of each gate. */
  private final List<ZkCircuitGate> gates;

  /** The addresses of the circuit root gates. */
  private List<ZkAddress> rootGateAddresses;

  /** Creates a new ZK circuit. */
  public ZkCircuitImpl() {
    this.gates = new ArrayList<>();
  }

  @Override
  public ZkCircuitGate getGate(final ZkAddress address) {
    return gates.get(address.address());
  }

  @Override
  public List<ZkAddress> getRoots() {
    return rootGateAddresses;
  }

  @Override
  public Stream<ZkAddress> gateAddresses() {
    return gates.stream().map(ZkCircuitGate::gateAddress);
  }

  /**
   * Set circuit root gates. The root is the gate used for computing the result of the circuit.
   *
   * @param rootGateAddresses The address of the root gate.
   * @exception NullPointerException If rootGateAddresses is null or contains null.
   */
  @Override
  public void setRoots(List<ZkAddress> rootGateAddresses) {
    this.rootGateAddresses = List.copyOf(rootGateAddresses);
  }

  /**
   * Add a gate to the circuit.
   *
   * @param operation The gate to add
   * @return The address of the newly added gate
   * @exception NullPointerException If the given operation is null.
   */
  @Override
  public ZkAddress add(final ZkType resultType, final ZkOperation operation) {
    final ZkAddress address = new ZkAddress(gates.size());
    gates.add(new ZkCircuitGate(address, resultType, operation));
    return address;
  }
}
