package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;

/** A zero-knowledge circuit capable of performing a zero-knowledge computation. */
public interface ZkCircuitAppendable extends ZkCircuit {

  /**
   * Set circuit root gates. The root is the gates used for computing the result of the circuit.
   *
   * @param rootGateAddresses The addresses of the root gates.
   */
  void setRoots(final List<ZkAddress> rootGateAddresses);

  /**
   * Add a {@link ZkCircuitGate} to the circuit.
   *
   * @param resultType Type of value produced by gate.
   * @param operation Operation of the added gate.
   * @return The address of the newly added gate
   * @exception NullPointerException If the given operation is null.
   */
  ZkAddress add(ZkType resultType, ZkOperation operation);
}
