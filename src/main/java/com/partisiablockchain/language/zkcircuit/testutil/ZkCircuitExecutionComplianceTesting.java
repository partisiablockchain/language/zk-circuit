package com.partisiablockchain.language.zkcircuit.testutil;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.language.zkcircuit.ZkAddress;
import com.partisiablockchain.language.zkcircuit.ZkCircuitExecution;
import com.partisiablockchain.language.zkcircuit.ZkOperation;
import com.partisiablockchain.language.zkcircuit.ZkType;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiPredicate;

/**
 * Compliance testing module for testing whether the given {@link ZkCircuitExecution} component is
 * implemented correctly.
 *
 * <p>The intention with {@link ZkCircuitExecutionComplianceTesting} is to allow projects
 * implementing {@link ZkCircuitExecution} to write a test using {@link
 * ZkCircuitExecutionComplianceTesting}, which will be automatically updated whenever a new {@link
 * ZkOperation} is added, or in case of some changes to the semantics.
 */
public final class ZkCircuitExecutionComplianceTesting {

  private ZkCircuitExecutionComplianceTesting() {}

  /**
   * Checks for {@link ZkCircuitExecution} non-environmental compliance.
   *
   * <p>Will check:
   *
   * <ul>
   *   <li>That {@link ZkCircuitExecution#executeGateConstant} produces different values for certain
   *       important constants.
   *   <li>That the rough interface for {@link ZkCircuitExecution#executeGateConstant}, {@link
   *       ZkCircuitExecution#executeGateUnary}, {@link ZkCircuitExecution#executeGateSignExtend},
   *       {@link ZkCircuitExecution#executeGatesBinary}, {@link
   *       ZkCircuitExecution#executeGateExtract} and {@link ZkCircuitExecution#executeGateSelect}
   *       is satisfied. Notably that result values must be non-null.
   *   <li>That all operations are supported for {@link ZkCircuitExecution#executeGateUnary} and
   *       {@link ZkCircuitExecution#executeGatesBinary}.
   *   <li>That {@link ZkCircuitExecution#executeGateSignExtend} produces values that are roughly
   *       consistent with the constant gate.
   * </ul>
   *
   * <p>Will not check: {@link ZkCircuitExecution#executeGateLoad}. This is assumed to be tested
   * separately.
   *
   * <p>The expected usage of this function is similar to: <code>
   * Assertions.assertThat(ZkCircuitExecutionComplianceTesting.testCompliance(execution)).isEmpty();
   * </code>
   *
   * @param <T> Type that the execution model is operating upon.
   * @param execution Execution model to check compliance for. Does not need to possess any
   *     environment.
   * @param equalityPredicate Equality predicate to use to compare whether values are equal.
   * @return List of problems.
   * @throws RuntimeException If execution model is not up to compliance.
   */
  @SuppressWarnings("PMD.EmptyCatchBlock")
  public static <T> List<String> testCompliance(
      final ZkCircuitExecution<T> execution, BiPredicate<T, T> equalityPredicate) {
    final List<String> complianceProblems = new ArrayList<>();

    final ZkAddress testAddr = new ZkAddress(1);

    // Constant
    final T testValueFalse =
        execution.executeGateConstant(ZkOperation.Constant.fromInt(ZkType.SBOOL, 0));
    final T testValueTrue =
        execution.executeGateConstant(ZkOperation.Constant.fromInt(ZkType.SBOOL, 1));

    final T testValueInteger0 =
        execution.executeGateConstant(ZkOperation.Constant.fromInt(ZkType.SBI32, 0));
    final T testValueInteger1 =
        execution.executeGateConstant(ZkOperation.Constant.fromInt(ZkType.SBI32, 1));
    final T testValueInteger2 =
        execution.executeGateConstant(ZkOperation.Constant.fromInt(ZkType.SBI32, 2));
    final T testValueIntegerNeg1 =
        execution.executeGateConstant(ZkOperation.Constant.fromInt(ZkType.SBI32, -1));

    final T testValueInteger3 =
        execution.executeGateConstant(ZkOperation.Constant.fromInt(ZkType.SBI32, 0x31233));
    final T testValueInteger4 =
        execution.executeGateConstant(ZkOperation.Constant.fromInt(ZkType.SBI32, 0x51231));

    requireNonNull(testValueFalse);
    requireNonNull(testValueTrue);
    requireNonNull(testValueInteger0);
    requireNonNull(testValueInteger1);
    requireNonNull(testValueInteger3);
    requireNonNull(testValueInteger4);

    if (equalityPredicate.test(testValueInteger0, testValueInteger2)) {
      complianceProblems.add("ZkOperation.Constant must not produce same value for 0 and 2");
    }

    // Unary
    for (final ZkOperation.UnaryOp binop : ZkOperation.UnaryOp.values()) {
      if (ZkOperation.UnaryOp.UNKNOWN == binop) {
        continue;
      }
      final ZkOperation.Unary gate = new ZkOperation.Unary(binop, testAddr);
      final T result = execution.executeGateUnary(gate, testValueFalse);
      requireNonNull(result);
    }

    // Unary, Unknown
    try {
      final var gate = new ZkOperation.Unary(ZkOperation.UnaryOp.UNKNOWN, testAddr);
      execution.executeGateUnary(gate, testValueFalse);
      // Incorrect if reached below statement
      complianceProblems.add(
          "ZkOperation.UnaryOp.UNKNOWN must throw UnsupportedOperationException");
    } catch (UnsupportedOperationException e) {
      // Correct
    }

    // Sign-extend
    final List<T> originalValues =
        List.of(
            testValueFalse,
            testValueTrue,
            testValueInteger0,
            testValueInteger1,
            testValueIntegerNeg1);
    final List<T> extendedValues =
        List.of(
            testValueInteger0,
            testValueIntegerNeg1,
            testValueInteger0,
            testValueInteger1,
            testValueIntegerNeg1);
    for (int idx = 0; idx < originalValues.size(); idx++) {
      final T originalValue = originalValues.get(idx);
      final T extendedValue = extendedValues.get(idx);

      final var gate = new ZkOperation.SignExtend(ZkType.SBI32, testAddr);
      T result = execution.executeGateSignExtend(gate, originalValue);
      if (!equalityPredicate.test(extendedValue, result)) {
        complianceProblems.add(
            "ZkOperation.SignExtend must convert %s to %s, not to %s"
                .formatted(originalValue, extendedValue, result));
      }
    }

    // Binary
    for (final ZkOperation.BinaryOp binop : ZkOperation.BinaryOp.values()) {
      if (ZkOperation.BinaryOp.UNKNOWN == binop) {
        continue;
      }
      final List<T> result =
          execution.executeGatesBinary(binop, List.of(testValueFalse), List.of(testValueFalse));
      requireNonNull(result);
      requireNonNull(result.get(0));
    }

    // Binary, Unknown
    try {
      execution.executeGatesBinary(
          ZkOperation.BinaryOp.UNKNOWN, List.of(testValueFalse), List.of(testValueFalse));
      // Incorrect if reached below statement
      complianceProblems.add(
          "ZkOperation.BinaryOp.UNKNOWN must throw UnsupportedOperationException");
    } catch (UnsupportedOperationException e) {
      // Correct
    }

    // Extract
    for (final int offset : List.of(0, 1, 3, 7, 30)) {
      final ZkOperation.Extract gate = new ZkOperation.Extract(testAddr, 1, offset);
      final T result = execution.executeGateExtract(gate, testValueInteger3);
      requireNonNull(result);
    }

    // Select
    for (final T testValueCnd : List.of(testValueTrue, testValueFalse)) {
      final var gate = new ZkOperation.Select(testAddr, testAddr, testAddr);
      final T result =
          execution.executeGateSelect(gate, testValueCnd, testValueInteger3, testValueInteger4);
      requireNonNull(result);
    }

    return complianceProblems;
  }
}
