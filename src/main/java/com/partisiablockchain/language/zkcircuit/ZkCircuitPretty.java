package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;
import java.util.Locale;

/** S-expression prettifier for {@link ZkCircuit}. */
public final class ZkCircuitPretty {
  private ZkCircuitPretty() {}

  /**
   * Creates prettified string for {@link ZkCircuit}.
   *
   * @param circuit Circuit to prettify.
   * @return Newly created string, never null.
   */
  public static String prettyCircuit(final ZkCircuit circuit) {
    // Format
    final StringBuilder out = new StringBuilder();
    out.append("(zkcircuit");
    circuit
        .gateAddresses()
        .forEachOrdered(
            address -> {
              out.append("\n  ");
              prettyGate(circuit.getGate(address), out);
            });

    out.append("\n  (output");

    for (final ZkAddress rootAddress : circuit.getRoots()) {
      out.append(" ").append(prettyAddress(rootAddress));
    }

    out.append("))");

    return out.toString();
  }

  private static void prettyGate(final ZkCircuitGate gate, final StringBuilder out) {
    out.append("(")
        .append(prettyType(gate.resultType()))
        .append(" ")
        .append(prettyAddress(gate.gateAddress()))
        .append(" ");
    prettyOperation(gate.operation(), out);
    out.append(")");
  }

  private static void prettyOperation(
      final ZkOperation uncheckedOperation, final StringBuilder out) {
    out.append("(");

    // Constant
    if (uncheckedOperation instanceof ZkOperation.Constant operation) {
      out.append(
          "constant %s %s"
              .formatted(
                  prettyType(operation.type()), formatBitsAsLiteral(operation.constantBits())));

      // SignExtend
    } else if (uncheckedOperation instanceof ZkOperation.SignExtend operation) {
      out.append(
          "sign_extend %s %s"
              .formatted(prettyType(operation.resultType()), prettyAddress(operation.addr1())));

      // Unary
    } else if (uncheckedOperation instanceof ZkOperation.Unary operation) {
      out.append(
          "%s %s"
              .formatted(
                  operation.operation().toString().toLowerCase(Locale.ENGLISH),
                  prettyAddress(operation.addr1())));

      // Binary
    } else if (uncheckedOperation instanceof ZkOperation.Binary operation) {
      out.append(
          "%s %s %s"
              .formatted(
                  operation.operation().toString().toLowerCase(Locale.ENGLISH),
                  prettyAddress(operation.addr1()),
                  prettyAddress(operation.addr2())));

      // Select
    } else if (uncheckedOperation instanceof ZkOperation.Select operation) {
      out.append(
          "select %s %s %s"
              .formatted(
                  prettyAddress(operation.addrCond()),
                  prettyAddress(operation.addrThen()),
                  prettyAddress(operation.addrElse())));

      // Extract
    } else if (uncheckedOperation instanceof ZkOperation.Extract operation) {
      out.append(
          "extract %s %d %d"
              .formatted(
                  prettyAddress(operation.addr1()), operation.bitwidth(), operation.bitOffset()));

      // Load
    } else {
      final ZkOperation.Load operation = (ZkOperation.Load) uncheckedOperation;
      out.append(
          "load %s %s"
              .formatted(prettyType(operation.type()), prettyVariableId(operation.variableId())));
    }

    out.append(")");
  }

  private static String prettyAddress(final ZkAddress address) {
    return "$%d".formatted(address.address());
  }

  private static String prettyVariableId(final ZkOperation.ZkVariableId variableId) {
    return "%%%d".formatted(variableId.variableId());
  }

  private static String prettyType(final ZkType uncheckedType) {
    final ZkType.Sbi type = (ZkType.Sbi) uncheckedType;
    return "sbi%d".formatted(type.bitwidth());
  }

  /**
   * Formats as given list of bits as a bitwise literal.
   *
   * @param bitsLittleEndian Bits with ascending significance.
   * @return Newly created string of literal. Never null.
   */
  public static String formatBitsAsLiteral(List<Boolean> bitsLittleEndian) {
    final StringBuilder out = new StringBuilder("0b");
    for (int idx = bitsLittleEndian.size() - 1; idx >= 0; idx--) {
      out.append(bitsLittleEndian.get(idx) ? "1" : "0");
    }
    return out.toString();
  }
}
