package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.google.errorprone.annotations.Immutable;

/**
 * ZK Gates that perform a computation on operands to create a result.
 *
 * @param gateAddress Address of gate in circuit.
 * @param resultType Type of value produced by gate.
 * @param operation Operation of the gate.
 */
@Immutable
public record ZkCircuitGate(ZkAddress gateAddress, ZkType resultType, ZkOperation operation) {

  /**
   * Constructor for new {@link ZkCircuitGate}.
   *
   * @param gateAddress Address of gate in circuit.
   * @param resultType Type of value produced by gate.
   * @param operation Operation of the gate.
   */
  public ZkCircuitGate {
    requireNonNull(gateAddress);
    requireNonNull(resultType);
    requireNonNull(operation);
  }
}
