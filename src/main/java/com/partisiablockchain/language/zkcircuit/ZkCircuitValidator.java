package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.Serial;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Checks whether a ZK circuit is valid. The validation ensures that all referenced gates exist and
 * performs type checking for all gates.
 */
public final class ZkCircuitValidator {

  private ZkCircuitValidator() {}

  /**
   * Validate a ZK circuit. Also checks that all gates of the circuit are ordered so any
   * dependencies appear before the gates that need them.
   *
   * <p>Root types will be checked if {@code expectedTypes} is non-null.
   *
   * @param circuit The circuit to validate. Must never be null.
   * @param expectedTypes The value types that the circuit computation is expected to produce. Null
   *     allowed.
   * @throws ZkCircuitInvalid If the circuit is invalid.
   */
  public static void validateZkCircuit(final ZkCircuit circuit, final List<ZkType> expectedTypes) {
    final ArrayList<ErrorLine> accumErrors = new ArrayList<>();

    // Validate circuit body
    final List<ZkType> rootOutputTypes = validateZkCircuitBody(circuit, accumErrors);

    // Validate output types
    if (expectedTypes != null) {
      validateOutputTypes(rootOutputTypes, expectedTypes, accumErrors);
    }

    // Build composite error message
    if (!accumErrors.isEmpty()) {
      throw new ZkCircuitInvalid(formatErrorMessages(accumErrors));
    }
  }

  /**
   * Validate a ZK circuit.
   *
   * @param circuit The circuit to validate.
   * @param accumErrors List to accumulate error messages to.
   */
  private static List<ZkType> validateZkCircuitBody(
      final ZkCircuit circuit, final List<ErrorLine> accumErrors) {
    final HashMap<ZkAddress, ZkType> seen = new HashMap<>();

    // Type check circuit
    circuit
        .gateAddresses()
        .map(circuit::getGate)
        .forEach(
            gate -> {
              final ZkType gateResultType =
                  validateGate(
                      gate,
                      seen,
                      message -> accumErrors.add(new ErrorLine(gate.gateAddress(), message)));
              seen.put(gate.gateAddress(), gateResultType);
            });

    // Type check roots
    final Consumer<String> addError = message -> accumErrors.add(new ErrorLine(null, message));
    return circuit.getRoots().stream()
        .map(gate -> assertGatePresent(seen, gate, addError))
        .toList();
  }

  /**
   * Validates that root output types and expected types are compatible.
   *
   * @param rootOutputTypes Types that roots produce. Never null.
   * @param expectedTypes The value types that the circuit computation is expected to produce. Never
   *     null.
   * @param accumErrors List to accumulate error messages to. Never null.
   */
  private static void validateOutputTypes(
      final List<ZkType> rootOutputTypes,
      final List<ZkType> expectedTypes,
      final List<ErrorLine> accumErrors) {

    final Consumer<String> addError = message -> accumErrors.add(new ErrorLine(null, message));
    if (expectedTypes.size() != rootOutputTypes.size()) {
      addError.accept(
          "Inconsistency between number of roots (%d), and expected output types (%d)"
              .formatted(expectedTypes.size(), rootOutputTypes.size()));
    }

    for (int idx = 0; idx < Integer.min(rootOutputTypes.size(), expectedTypes.size()); idx++) {
      assertEqualType(expectedTypes.get(idx), rootOutputTypes.get(idx), addError);
    }
  }

  private static String formatErrorMessages(Iterable<ErrorLine> errors) {
    final StringBuilder builder = new StringBuilder();
    builder.append("Could not validate ZkCircuit, found following errors:");
    for (final ErrorLine error : errors) {
      final String where =
          error.address() != null ? "Gate " + error.address().address() : "Circuit";
      builder.append("\n  %-8s: %s".formatted(where, error.message()));
    }
    return builder.toString();
  }

  /**
   * Validates individual {@link ZkCircuitGate}.
   *
   * @param gate Gate to validate.
   * @param alreadyValidatedGates Map with types of already validated gates.
   * @param addError Function to call to register new validation errors.
   * @return Type of value produced by operation.
   */
  private static ZkType validateGate(
      final ZkCircuitGate gate,
      final Map<ZkAddress, ZkType> alreadyValidatedGates,
      final Consumer<String> addError) {
    final ZkType producedType =
        validateOperation(gate.operation(), alreadyValidatedGates, addError);
    if (!producedType.equals(gate.resultType())) {
      addError.accept(
          "Expected type %s, but operation produced type %s"
              .formatted(gate.resultType().pretty(), producedType.pretty()));
    }
    return gate.resultType();
  }

  /**
   * Validates individual operations.
   *
   * @param uncheckedOperation Operation to error check.
   * @param alreadyValidatedGates Map with types of already validated gates.
   * @param addError Function to call to register new validation errors.
   * @return Type of value produced by operation.
   */
  private static ZkType validateOperation(
      final ZkOperation uncheckedOperation,
      final Map<ZkAddress, ZkType> alreadyValidatedGates,
      final Consumer<String> addError) {

    // Constant gates
    if (uncheckedOperation instanceof ZkOperation.Constant gate) {
      assertBitsInType(gate.constantBits(), gate.type(), addError);
      return gate.type();

      // Load Variable gate
    } else if (uncheckedOperation instanceof ZkOperation.Load gate) {
      return gate.type();

      // Unary operation gates
    } else if (uncheckedOperation instanceof ZkOperation.Unary gate) {
      // All current operations map to the same type.
      final ZkType addr1Type = assertGatePresent(alreadyValidatedGates, gate.addr1(), addError);

      return addr1Type;

      // SignExtend operation gates
    } else if (uncheckedOperation instanceof ZkOperation.SignExtend gate) {
      // All current operations map to the same type.
      final ZkType addr1Type = assertGatePresent(alreadyValidatedGates, gate.addr1(), addError);
      final int outputBitwidth = ((ZkType.Sbi) gate.resultType()).bitwidth();
      final int inputBitwidth = ((ZkType.Sbi) addr1Type).bitwidth();

      if (outputBitwidth < inputBitwidth) {
        addError.accept(
            "Sign-extend input type %s must not be larger than output type %s"
                .formatted(addr1Type.pretty(), gate.resultType().pretty()));
      }

      return gate.resultType();

      // Concat operation gate
    } else if (uncheckedOperation instanceof ZkOperation.Binary gate
        && gate.operation() == ZkOperation.BinaryOp.BIT_CONCAT) {
      final ZkType addr1Type = assertGatePresent(alreadyValidatedGates, gate.addr1(), addError);
      final ZkType addr2Type = assertGatePresent(alreadyValidatedGates, gate.addr2(), addError);
      final int outputBitwidth =
          ((ZkType.Sbi) addr1Type).bitwidth() + ((ZkType.Sbi) addr2Type).bitwidth();
      return new ZkType.Sbi(outputBitwidth);

      // Binary operation gates
    } else if (uncheckedOperation instanceof ZkOperation.Binary gate) {
      final ZkType addr1Type = assertGatePresent(alreadyValidatedGates, gate.addr1(), addError);
      final ZkType addr2Type = assertGatePresent(alreadyValidatedGates, gate.addr2(), addError);

      assertEqualType(addr1Type, addr2Type, addError);
      return CONDITIONAL_OPERATIONS.contains(gate.operation()) ? ZkType.SBOOL : addr1Type;

      // Extract operation gate
    } else if (uncheckedOperation instanceof ZkOperation.Extract gate) {
      final ZkType addr1Type = assertGatePresent(alreadyValidatedGates, gate.addr1(), addError);
      if (gate.bitOffset() < 0) {
        addError.accept("Bit offset %d should be non-negative".formatted(gate.bitwidth()));
      }
      if (gate.bitwidth() < 0) {
        addError.accept("Bit width %d should be non-negative".formatted(gate.bitwidth()));
      }
      final int bitwidth = ((ZkType.Sbi) addr1Type).bitwidth();
      if (bitwidth < gate.bitOffset() + gate.bitwidth()) {
        addError.accept(
            "Extracted bit range [%d,%d[, extends outside %d-bit input"
                .formatted(gate.bitOffset(), gate.bitOffset() + gate.bitwidth(), bitwidth));
      }
      return new ZkType.Sbi(Math.max(0, gate.bitwidth()));

      // Select operation
    } else {
      final ZkOperation.Select gate = (ZkOperation.Select) uncheckedOperation;
      final ZkType addrCondType =
          assertGatePresent(alreadyValidatedGates, gate.addrCond(), addError);
      final ZkType addrThenType =
          assertGatePresent(alreadyValidatedGates, gate.addrThen(), addError);
      final ZkType addrElseType =
          assertGatePresent(alreadyValidatedGates, gate.addrElse(), addError);

      assertEqualType(ZkType.SBOOL, addrCondType, addError);
      assertEqualType(addrThenType, addrElseType, addError);
      return addrThenType;
    }
  }

  private static Set<ZkOperation.BinaryOp> CONDITIONAL_OPERATIONS =
      Set.of(
          ZkOperation.BinaryOp.CMP_LESS_THAN_SIGNED,
          ZkOperation.BinaryOp.CMP_LESS_THAN_OR_EQUALS_SIGNED,
          ZkOperation.BinaryOp.CMP_LESS_THAN_UNSIGNED,
          ZkOperation.BinaryOp.CMP_LESS_THAN_OR_EQUALS_UNSIGNED,
          ZkOperation.BinaryOp.CMP_EQUALS);

  private static ZkType assertGatePresent(
      final Map<ZkAddress, ZkType> alreadyValidatedGates,
      final ZkAddress address,
      final Consumer<String> addError) {
    final ZkType type = alreadyValidatedGates.get(address);
    if (type != null) {
      return type;
    }

    addError.accept("Reference to unknown gate %d".formatted(address.address()));
    return ZkType.SBOOL;
  }

  private static void assertEqualType(
      final ZkType typeExpected, final ZkType typeGotten, final Consumer<String> addError) {
    if (!typeExpected.equals(typeGotten)) {
      addError.accept(
          "Expected type %s, but got type %s"
              .formatted(typeExpected.pretty(), typeGotten.pretty()));
    }
  }

  private static void assertBitsInType(
      final List<Boolean> bits, final ZkType type, final Consumer<String> addError) {
    final int typeBitwidth = ((ZkType.Sbi) type).bitwidth();
    if (bits.size() != typeBitwidth) {
      addError.accept(
          "Constant %s does not fit type %s"
              .formatted(ZkCircuitPretty.formatBitsAsLiteral(bits), type.pretty()));
    }
  }

  @SuppressWarnings("UnusedVariable")
  private record ErrorLine(ZkAddress address, String message) {}

  /** Exception thrown by the ZK circuit validator if the circuit is invalid. */
  public static final class ZkCircuitInvalid extends RuntimeException {

    @Serial private static final long serialVersionUID = 1897642393284462193L;

    /**
     * Create the ZK circuit invalid exception.
     *
     * @param message The message.
     */
    public ZkCircuitInvalid(String message) {
      super(message);
    }
  }
}
