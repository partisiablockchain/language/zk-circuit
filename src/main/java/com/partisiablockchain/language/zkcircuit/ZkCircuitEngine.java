package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Core circuit execution engine. Abstracted over the type of the executed values, in order to allow
 * test executers.
 */
public record ZkCircuitEngine<ValueT>(ZkCircuitExecution<ValueT> execution, ZkCircuit circuit) {

  /**
   * Constructor.
   *
   * @param execution Handles manipulation of realized values.
   * @param circuit Circuit to execute over.
   */
  public ZkCircuitEngine {
    Objects.requireNonNull(execution);
    Objects.requireNonNull(circuit);
  }

  /**
   * Execute a ZK circuit. The gates are executed in order, so this requires that all gates of the
   * circuit to be ordered so any dependencies appear before the gates that need them.
   *
   * @return List of resulting values. Never null.
   */
  public List<ValueT> executeZkCircuit() {
    final HashMap<ZkAddress, ValueT> gateResults = new HashMap<>();

    final ArrayList<ZkCircuitGate> binaryGatesAccum = new ArrayList<>();

    circuit()
        .gateAddresses()
        .forEachOrdered(
            address -> {
              final ZkCircuitGate gate = circuit.getGate(address);
              if (gate.operation() instanceof ZkOperation.Binary operation) {
                // Check whether there is a need to flush
                if (flushBinaryAccumNeededBeforeOperation(
                    gate.resultType(), operation, binaryGatesAccum, gateResults)) {
                  executeBinaryGates(binaryGatesAccum, gateResults);
                }
                binaryGatesAccum.add(gate);
              } else {
                executeBinaryGatesIfAny(binaryGatesAccum, gateResults);
                final ValueT gateResultType = executeOperation(gate.operation(), gateResults);
                gateResults.put(address, gateResultType);
              }
            });

    executeBinaryGatesIfAny(binaryGatesAccum, gateResults);

    return circuit.getRoots().stream().map(gateResults::get).map(Objects::requireNonNull).toList();
  }

  /**
   * Determines whether there is a need to flush the binary accumulator before being able to execute
   * an upcoming operation.
   *
   * @param resultType Result type of operation.
   * @param operation Operation.
   * @param gateResults Results of previously executed gates. Not modified.
   * @param binaryGates The binary operation accumulator. Not modified.
   * @return Returns true when the operation requires flushing the binops.
   */
  private static boolean flushBinaryAccumNeededBeforeOperation(
      final ZkType resultType,
      final ZkOperation.Binary operation,
      final List<ZkCircuitGate> binaryGates,
      final Map<ZkAddress, ?> gateResults) {
    if (!gateResults.containsKey(operation.addr1())) {
      return true;
    }
    if (!gateResults.containsKey(operation.addr2())) {
      return true;
    }
    if (binaryGates.isEmpty()) {
      return false;
    }
    final boolean isSameType = resultType.equals(binaryGates.get(0).resultType());
    final boolean isSameOperation =
        ((ZkOperation.Binary) binaryGates.get(0).operation()).operation() == operation.operation();
    return !isSameType || !isSameOperation;
  }

  private void executeBinaryGatesIfAny(
      List<ZkCircuitGate> binaryGates, Map<ZkAddress, ValueT> gateResults) {
    if (binaryGates.isEmpty()) {
      return;
    }
    executeBinaryGates(binaryGates, gateResults);
  }

  /**
   * Executes the accumulated {@link ZkOperation.Binary} operations, in the given environment.
   * Assumes that all gates that the operation depends on have already been executed. All operations
   * are dispatched at the same time, allowing the {@link ZkCircuitExecution} to choose whether to
   * implement them in parallel or as singular instructions.
   *
   * @param binaryGates The binary operation accumulator to execute. Emptied afterwards.
   * @param gateResults Results of previously executed gates. Extended with results of the executed
   *     gates.
   */
  private void executeBinaryGates(
      final List<ZkCircuitGate> binaryGates, final Map<ZkAddress, ValueT> gateResults) {
    // Collect arguments
    final List<ZkOperation.Binary> operations =
        binaryGates.stream().map(x -> (ZkOperation.Binary) x.operation()).toList();
    final ZkOperation.BinaryOp binop = operations.get(0).operation();
    final List<ValueT> addr1Results =
        operations.stream().map(ZkOperation.Binary::addr1).map(gateResults::get).toList();
    final List<ValueT> addr2Results =
        operations.stream().map(ZkOperation.Binary::addr2).map(gateResults::get).toList();

    // Execute gates
    final List<ValueT> results = execution().executeGatesBinary(binop, addr1Results, addr2Results);

    // Extract results
    for (int idx = 0; idx < binaryGates.size(); idx++) {
      gateResults.put(binaryGates.get(idx).gateAddress(), results.get(idx));
    }

    // Cleanup
    binaryGates.clear();
  }

  /**
   * Executes individual zk operation, in the given environment. Assumes that all gates that the
   * operation depends on have already been executed.
   *
   * @param uncheckedOperation Operation to perform. Never null.
   * @param gateResults Result values for other gates; used by some operations to compute result.
   *     Never null.
   * @return Operation result.
   */
  private ValueT executeOperation(
      final ZkOperation uncheckedOperation, final Map<ZkAddress, ValueT> gateResults) {

    // Constant gates
    if (uncheckedOperation instanceof ZkOperation.Constant gate) {
      return execution().executeGateConstant(gate);

      // Load Variable gate
    } else if (uncheckedOperation instanceof ZkOperation.Load gate) {
      return execution().executeGateLoad(gate);

      // Unary operation gates
    } else if (uncheckedOperation instanceof ZkOperation.Unary gate) {
      final ValueT addr1Result = gateResults.get(gate.addr1());
      return execution().executeGateUnary(gate, addr1Result);

      // Sign-extension operation gates
    } else if (uncheckedOperation instanceof ZkOperation.SignExtend gate) {
      final ValueT addr1Result = gateResults.get(gate.addr1());
      return execution().executeGateSignExtend(gate, addr1Result);

      // Extract operation gate
    } else if (uncheckedOperation instanceof ZkOperation.Extract gate) {
      final ValueT addr1Result = gateResults.get(gate.addr1());
      return execution().executeGateExtract(gate, addr1Result);

      // Select operation
    } else {
      final ZkOperation.Select gate = (ZkOperation.Select) uncheckedOperation;
      final ValueT addrCondType = gateResults.get(gate.addrCond());
      final ValueT addrThenType = gateResults.get(gate.addrThen());
      final ValueT addrElseType = gateResults.get(gate.addrElse());
      return execution().executeGateSelect(gate, addrCondType, addrThenType, addrElseType);
    }
  }
}
