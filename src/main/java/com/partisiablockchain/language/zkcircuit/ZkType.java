package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;

/** The different types of values that can move through a {@link ZkCircuit}. */
@Immutable
public sealed interface ZkType {

  /**
   * Produces pretty string for type.
   *
   * @return Pretty string. Never null.
   */
  String pretty();

  /** Secret-shared unit. Mostly Useless */
  Sbi SBI0 = new Sbi(0);

  /** Secret-shared binary boolean. */
  Sbi SBOOL = new Sbi(1);

  /** Secret-shared binary 32-bit signed integer. */
  Sbi SBI32 = new Sbi(32);

  /** Secret-shared binary N-bit signed integer. */
  @Immutable
  record Sbi(int bitwidth) implements ZkType {
    /**
     * Constructor.
     *
     * @param bitwidth Number of bits in values of type. Must be non-negative.
     */
    public Sbi {
      if (bitwidth < 0) {
        throw new IllegalArgumentException(
            "Cannot produce a type with negative bitwidth %d".formatted(bitwidth));
      }
    }

    @Override
    public String pretty() {
      return "SBI%d".formatted(bitwidth);
    }
  }
}
