package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.google.errorprone.annotations.Immutable;
import java.util.ArrayList;
import java.util.List;

/** ZK Gates that perform a computation on operands to create a result. */
@Immutable
public sealed interface ZkOperation {

  /** Id of a zk variable. */
  @Immutable
  record ZkVariableId(int variableId) {}

  /** Constant value. */
  @Immutable
  @SuppressWarnings("Immutable")
  record Constant(ZkType.Sbi type, List<Boolean> constantBits) implements ZkOperation {

    /**
     * Create {@link Constant} operation.
     *
     * @param type Type to load constant as.
     * @param constantBits Bits of the constant, as a list of bits, little endian.
     */
    public Constant {
      requireNonNull(type);
      requireNonNull(constantBits);
    }

    /**
     * Create {@link Constant} operation based on the given integer.
     *
     * @param type Type to load constant as.
     * @param intConstant Integer constant to extract bits from for constant.
     * @return Newly created {@link Constant} operation.
     */
    public static Constant fromInt(final ZkType.Sbi type, int intConstant) {
      final List<Boolean> bits = new ArrayList<>();
      while (bits.size() < type.bitwidth()) {
        bits.add((intConstant & 1) != 0);
        intConstant >>>= 1;
      }
      return new Constant(type, List.copyOf(bits));
    }
  }

  /** Unary operation, over a single input. */
  @Immutable
  record Unary(UnaryOp operation, ZkAddress addr1) implements ZkOperation {

    /**
     * Constructor.
     *
     * @param operation Operation to perform.
     * @param addr1 Address of condition first and only argument input.
     */
    public Unary {
      requireNonNull(operation);
      requireNonNull(addr1);
    }
  }

  /**
   * Arithmetic sign extension of signed number.
   *
   * @param resultType Type to sign extend into.
   * @param addr1 Address of condition first and only argument input.
   */
  @Immutable
  record SignExtend(ZkType resultType, ZkAddress addr1) implements ZkOperation {

    /**
     * Constructor.
     *
     * @param resultType Type to sign extend into.
     * @param addr1 Address of condition first and only argument input.
     */
    public SignExtend {
      requireNonNull(resultType);
      requireNonNull(addr1);
    }
  }

  /** Binary operation, over a two inputs. */
  @Immutable
  record Binary(BinaryOp operation, ZkAddress addr1, ZkAddress addr2) implements ZkOperation {

    /**
     * Constructor.
     *
     * @param operation Operation to perform.
     * @param addr1 Address of condition first argument input.
     * @param addr2 Address of condition second argument input.
     */
    public Binary {
      requireNonNull(operation);
      requireNonNull(addr1);
      requireNonNull(addr2);
    }

    /**
     * Creates a new normalized {@link ZkOperation.Binary}.
     *
     * <p>This method is preferable to using the {@link ZkOperation.Binary} constructor, as this
     * method will attempt to perform some normalization that might improves the hit rate of common
     * expression elimination algorithms.
     *
     * <p>Normalizations:
     *
     * <ul>
     *   <li>Ensures {@link ZkAddress} arguments are in rising order, for commutative operations.
     * </ul>
     *
     * @param operation Operation to perform.
     * @param addr1 Address of condition first argument input.
     * @param addr2 Address of condition second argument input.
     * @return Normalized binary operation.
     */
    public static Binary newNormalized(
        final ZkOperation.BinaryOp operation, ZkAddress addr1, ZkAddress addr2) {
      if (operation.isCommutative() && addr2.address() < addr1.address()) {
        return new Binary(operation, addr2, addr1);
      } else {
        return new Binary(operation, addr1, addr2);
      }
    }
  }

  /** Ternary operation, choosing between two inputs based on the first inputs. */
  @Immutable
  record Select(ZkAddress addrCond, ZkAddress addrThen, ZkAddress addrElse) implements ZkOperation {

    /**
     * Constructor.
     *
     * @param addrCond Address of condition input.
     * @param addrThen Address of condition then case input.
     * @param addrElse Address of condition else case input.
     */
    public Select {
      requireNonNull(addrCond);
      requireNonNull(addrThen);
      requireNonNull(addrElse);
    }
  }

  /** Operation for taking a number of bits out of the input. */
  @Immutable
  record Extract(ZkAddress addr1, int bitwidth, int bitOffset) implements ZkOperation {

    /**
     * Constructor for Extract operation.
     *
     * @param addr1 Address of input variable.
     * @param bitwidth Number bits to extract.
     * @param bitOffset Number bits offset from low bits.
     */
    public Extract {
      requireNonNull(addr1);
    }
  }

  /** Loads variable data. */
  @Immutable
  record Load(ZkType type, ZkVariableId variableId) implements ZkOperation {

    /**
     * Constructor.
     *
     * @param type Type to load variable as.
     * @param variableId Id of variable to load.
     */
    public Load {
      requireNonNull(type);
      requireNonNull(variableId);
    }
  }

  /**
   * Unary operations.
   *
   * @see ZkOperation.Unary
   */
  enum UnaryOp {
    /** Bitwise not. */
    BITWISE_NOT,
    /** Arithmetic negation of a signed number. */
    ARITH_NEGATE_SIGNED,
    /** Used to test for unknown operation handling. */
    UNKNOWN
  }

  /**
   * Binary operations.
   *
   * <p>Operations are classified based on their properties; whether those are associative or
   * commutative.
   *
   * @see ZkOperation.Binary
   */
  enum BinaryOp {
    /** Bitwise and. */
    BITWISE_AND(true),
    /** Bitwise or. */
    BITWISE_OR(true),
    /** Bitwise xor. */
    BITWISE_XOR(true),
    /** Arithmetic multiplication of signed numbers. */
    ARITH_MULTIPLY_SIGNED(true),
    /** Arithmetic addition of two numbers; works for both signed and unsigned. */
    ARITH_ADD_WRAPPING(true),
    /** Arithmetic subtraction of two numbers; works for both signed and unsigned. */
    ARITH_SUBTRACT_WRAPPING(false),
    /**
     * Bit: Concat two inputs side by side.
     *
     * <p>Is not marked as associative. The underlying operation is associative, but by moving the
     * parenthesis we are changing the intermediate types.
     */
    BIT_CONCAT(false),
    /** Less-than comparison of two signed numbers. */
    CMP_LESS_THAN_SIGNED(false),
    /** Less-than-or-equals comparison of two signed numbers. */
    CMP_LESS_THAN_OR_EQUALS_SIGNED(false),
    /** Less-than comparison of two signed numbers. */
    CMP_LESS_THAN_UNSIGNED(false),
    /** Less-than-or-equals comparison of two signed numbers. */
    CMP_LESS_THAN_OR_EQUALS_UNSIGNED(false),
    /** Equality comparison. */
    CMP_EQUALS(true),
    /** Used to test for unknown operation handling. */
    UNKNOWN(false);

    private final boolean associativeAndCommutative;

    BinaryOp(boolean associativeAndCommutative) {
      this.associativeAndCommutative = associativeAndCommutative;
    }

    /**
     * True if and only if the operation is both associative and commutative without changing types.
     *
     * <p>This property is merged, as there is no supported operation that is only associative or
     * only commutative, which makes it difficult to satisfy pitest when doing conditionals
     * involving both properties.
     *
     * @return True if and only if the operation is associative and commutative.
     * @see <a href="https://en.wikipedia.org/wiki/Associative_property">Associative property,
     *     Wikipedia</a>
     * @see <a href="https://en.wikipedia.org/wiki/Commutative_property">Commutative property,
     *     Wikipedia</a>
     */
    public boolean isAssociativeAndCommutative() {
      return associativeAndCommutative;
    }

    /**
     * True if and only if the operation is associative without changing types.
     *
     * @return True if and only if the operation is associative.
     * @see <a href="https://en.wikipedia.org/wiki/Associative_property">Associative property,
     *     Wikipedia</a>
     */
    public boolean isAssociative() {
      return isAssociativeAndCommutative();
    }

    /**
     * True if and only if the operation is commutative.
     *
     * @return True if and only if the operation is commutative.
     * @see <a href="https://en.wikipedia.org/wiki/Commutative_property">Commutative property,
     *     Wikipedia</a>
     */
    public boolean isCommutative() {
      return isAssociativeAndCommutative();
    }
  }
}
