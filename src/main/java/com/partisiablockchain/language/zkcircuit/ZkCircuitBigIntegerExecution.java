package com.partisiablockchain.language.zkcircuit;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * An insecure executor capable of executing a ZK circuit, using public {@link BigInteger}
 * variables. Meant for testing zk operations.
 */
public record ZkCircuitBigIntegerExecution(Map<Integer, BigInteger> variableStorage)
    implements ZkCircuitExecution<ZkCircuitBigIntegerExecution.Value> {

  /**
   * Create new execution model.
   *
   * @param variableStorage Variable storage.
   */
  public ZkCircuitBigIntegerExecution {
    requireNonNull(variableStorage);
  }

  /** Internal value of the execution model. */
  public record Value(BigInteger value, int bitwidth) {
    /**
     * Create new value.
     *
     * @param value The {@link BigInteger} value of the value.
     * @param bitwidth Bitwidth of the value.
     */
    public Value {
      requireNonNull(value);
      if (value.signum() < 0) {
        throw new IllegalArgumentException(
            "Value %s should have been non-negative".formatted(value));
      }
      if (value.bitLength() > bitwidth) {
        throw new IllegalArgumentException(
            "Value %s cannot fit into %d bits".formatted(value, bitwidth));
      }
    }
  }

  static final Value TRUE = new Value(BigInteger.ONE, 1);
  static final Value FALSE = new Value(BigInteger.ZERO, 1);

  @Override
  public Value executeGateConstant(final ZkOperation.Constant gate) {
    final BigInteger sum = constantUnsigned(gate.constantBits());
    return fitInType(sum, gate.type());
  }

  /**
   * Convert a list of bits to an unsigned {@link BigInteger}.
   *
   * @param bits List of bits. The least significant bit is at index 0.
   * @return The {@link BigInteger} number
   */
  static BigInteger constantUnsigned(final List<Boolean> bits) {
    final byte[] bytes = allocateByteArrayFromBits(bits.size());
    // BigInteger expects big-endian bytes
    int byteIdx = bytes.length - 1;
    int bit = 1;
    for (Boolean constantBit : bits) {
      bytes[byteIdx] ^= (byte) (constantBit ? bit : 0);
      bit <<= 1;
      if (bit == 256) {
        bit = 1;
        byteIdx--;
      }
    }
    return new BigInteger(1, bytes);
  }

  /**
   * Allocates a byte array large enough for the given number of bits.
   *
   * @param numBits Number to bits to allocate for.
   * @return Allocated array
   */
  static byte[] allocateByteArrayFromBits(int numBits) {
    final int numBytes = (numBits + 7) / 8;
    return new byte[numBytes];
  }

  @Override
  public Value executeGateLoad(final ZkOperation.Load gate) {
    final BigInteger value = variableStorage().get(gate.variableId().variableId());
    return fitInType(value, gate.type());
  }

  @Override
  public Value executeGateSignExtend(final ZkOperation.SignExtend gate, final Value input1) {
    final int expectedBitwidth = ((ZkType.Sbi) gate.resultType()).bitwidth();
    final int gottenBitwidth = input1.bitwidth();
    final int extensionBitwidth = expectedBitwidth - gottenBitwidth;

    final boolean isSigned = input1.value().testBit(gottenBitwidth - 1);
    final List<Boolean> signExtensionBits =
        IntStream.range(0, extensionBitwidth).mapToObj(i -> isSigned).toList();
    final Value signExtension =
        fitInType(constantUnsigned(signExtensionBits), signExtensionBits.size());
    return concat(signExtension, input1);
  }

  @Override
  public Value executeGateUnary(final ZkOperation.Unary gate, final Value input1) {
    // Bitwise not
    if (gate.operation() == ZkOperation.UnaryOp.BITWISE_NOT) {
      return fitInType(input1.value().not(), input1.bitwidth());

      // Arithmetic negate sign
    } else if (gate.operation() == ZkOperation.UnaryOp.ARITH_NEGATE_SIGNED) {
      return fitInType(input1.value().negate(), input1.bitwidth());

      // Unknown
    } else {
      throw new UnsupportedOperationException("Unknown unop: " + gate.operation());
    }
  }

  @Override
  public List<Value> executeGatesBinary(
      final ZkOperation.BinaryOp binop, final List<Value> inputs1, final List<Value> inputs2) {
    final List<Value> result = new ArrayList<>();
    for (int idx = 0; idx < inputs1.size(); idx++) {
      result.add(executeGateBinary(binop, inputs1.get(idx), inputs2.get(idx)));
    }
    return result;
  }

  private Value executeGateBinary(
      final ZkOperation.BinaryOp binop, final Value input1, final Value input2) {

    // Bitwise and
    if (binop == ZkOperation.BinaryOp.BITWISE_AND) {
      return fitInType(input1.value().and(input2.value()), input1.bitwidth());

      // Bitwise or
    } else if (binop == ZkOperation.BinaryOp.BITWISE_OR) {
      return fitInType(input1.value().or(input2.value()), input1.bitwidth());

      // Bitwise xor
    } else if (binop == ZkOperation.BinaryOp.BITWISE_XOR) {
      return fitInType(input1.value().xor(input2.value()), input1.bitwidth());

      // Bit: Concat
    } else if (binop == ZkOperation.BinaryOp.BIT_CONCAT) {
      return concat(input1, input2);

      // Arithmetic multiplication signed
    } else if (binop == ZkOperation.BinaryOp.ARITH_MULTIPLY_SIGNED) {
      return fitInType(toSigned(input1).multiply(toSigned(input2)), input1.bitwidth());

      // Arithmetic addition
    } else if (binop == ZkOperation.BinaryOp.ARITH_ADD_WRAPPING) {
      return fitInType(input1.value().add(input2.value()), input1.bitwidth());

      // Arithmetic subtraction
    } else if (binop == ZkOperation.BinaryOp.ARITH_SUBTRACT_WRAPPING) {
      return fitInType(input1.value().subtract(input2.value()), input1.bitwidth());

      // Comparison: Less Than
    } else if (binop == ZkOperation.BinaryOp.CMP_LESS_THAN_SIGNED) {
      final BigInteger value1Signed = toSigned(input1);
      final BigInteger value2Signed = toSigned(input2);
      return value1Signed.compareTo(value2Signed) < 0 ? TRUE : FALSE;

      // Comparison: Less than or equals
    } else if (binop == ZkOperation.BinaryOp.CMP_LESS_THAN_OR_EQUALS_SIGNED) {
      final BigInteger value1Signed = toSigned(input1);
      final BigInteger value2Signed = toSigned(input2);
      return value1Signed.compareTo(value2Signed) <= 0 ? TRUE : FALSE;

      // Comparison: Less Than
    } else if (binop == ZkOperation.BinaryOp.CMP_LESS_THAN_UNSIGNED) {
      return input1.value().compareTo(input2.value()) < 0 ? TRUE : FALSE;

      // Comparison: Less than or equals
    } else if (binop == ZkOperation.BinaryOp.CMP_LESS_THAN_OR_EQUALS_UNSIGNED) {
      return input1.value().compareTo(input2.value()) <= 0 ? TRUE : FALSE;

      // Comparison: Equals
    } else if (binop == ZkOperation.BinaryOp.CMP_EQUALS) {
      return input1.value().compareTo(input2.value()) == 0 ? TRUE : FALSE;

      // Unknown
    } else {
      throw new UnsupportedOperationException("Unknown binop: " + binop);
    }
  }

  @Override
  public Value executeGateSelect(
      final ZkOperation.Select gate,
      final Value inputCond,
      final Value inputThen,
      final Value inputElse) {
    return !inputCond.value().equals(BigInteger.ZERO) ? inputThen : inputElse;
  }

  @Override
  public Value executeGateExtract(final ZkOperation.Extract gate, final Value value) {
    return fitInType(value.value().shiftRight(gate.bitOffset()), gate.bitwidth());
  }

  private static BigInteger toSigned(final Value value) {
    return toSigned(value.value(), value.bitwidth());
  }

  static BigInteger toSigned(final BigInteger unsignedValue, int bitwidth) {
    final BigInteger minusMax = BigInteger.ONE.shiftLeft(bitwidth - 1);
    return unsignedValue.compareTo(minusMax) >= 0
        ? unsignedValue.subtract(minusMax.shiftLeft(1))
        : unsignedValue;
  }

  private static Value fitInType(final BigInteger number, final ZkType uncheckedType) {
    return fitInType(number, typeBitwidth(uncheckedType));
  }

  private static Value fitInType(final BigInteger number, final int bitwidth) {
    final BigInteger maxPlusOne = BigInteger.ONE.shiftLeft(bitwidth);
    return new Value(number.add(maxPlusOne).remainder(maxPlusOne), bitwidth);
  }

  private static int typeBitwidth(final ZkType uncheckedType) {
    final ZkType.Sbi type = (ZkType.Sbi) uncheckedType;
    return type.bitwidth();
  }

  private static Value concat(final Value inputUpper, final Value inputLower) {
    final BigInteger packed =
        inputUpper.value().shiftLeft(inputLower.bitwidth()).xor(inputLower.value());
    return fitInType(packed, inputUpper.bitwidth() + inputLower.bitwidth());
  }
}
